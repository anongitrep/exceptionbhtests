from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017/')
db = client['ExceptionHunter']
collection = db['ExceptionDB']

projectsWithoutExceptionTests = 0
projectsWithoutExceptionUses = 0
for doc in collection.find():
    dropDocument = False

    if doc["statistics"]['totalNumberOfCustomExceptionsUses'] == 0 and doc["statistics"][
        'totalNumberOfStandardOrThirdPartyExceptionsUses'] == 0:
        projectsWithoutExceptionUses += 1
        dropDocument = True
        continue

    if doc['statistics']["totalNumberOfTestMethods"] == 0:
        projectsWithoutExceptionTests += 1
        dropDocument = True
        continue



print(f'Projects Without Exceptional Behavior Tests: {projectsWithoutExceptionTests}')
print(f'Projects Without Exceptions Uses Tests: {projectsWithoutExceptionUses}')