import os
import math
from pathlib import Path
from datetime import datetime, timedelta
from calendar import monthrange
from scipy import stats
from numpy import percentile


# SIZE_AXES_LABELS = 12
# SIZE_AXES_VALUES = 10
# SIZE_AXES_TITLE = 12
# SIZE_BAR_VALUES = 10
# SIZE_LEGEND_TITLE = 12
# SIZE_LEGEND_TEXT = 10

SIZE_AXES_LABELS = "large"
SIZE_AXES_VALUES = "medium"
SIZE_AXES_TITLE = "large"
SIZE_BAR_VALUES = "medium"
SIZE_LEGEND_TITLE = "large"
SIZE_LEGEND_TEXT = "medium"


# print (sns.color_palette().as_hex())

# COLOR_PALETTE = {
#     "Frameworks": "#1f77b4",
#     "Libraries": "#ff7f0e",
#     "Tools": "#2ca02c",
#     "Desktop/Server": "#53afc9",
#     "Mobile": "#94d1bf",
#     "Multi-platform": "#2ca02c"
# }

# COLOR_PALETTE = {
#     "Frameworks": "#1f77b4",
#     "Libraries": "#ff7f0e",
#     "Tools": "#2ca02c",
#     "Desktop/Server": "#a6cee3",
#     "Mobile": "#ffff99",
#     "Multi-platform": "#fdbf6f"
# }


COLOR_PALETTE = {
    "Frameworks": "#1f77b4",
    "Libraries": "#ff7f0e",
    "Tools": "#2ca02c",
    "Desktop/Server": "#1eae28",
    "Mobile": "#f25e5a",
    "Multi-platform": "#4c88ff"
}

LABELS = ["ZERO", "> ZERO"]
COLOR_DEGRADE = {
    LABELS[0]: "#abd0e6",
    LABELS[1]: "#1b69af",
}

FIGURE_ORDER = ["Desktop/Server", "Mobile", "Multi-platform"]

def createDirs(path):
    output_path = Path(path)
    output_path.mkdir(parents=True, exist_ok=True)
    return output_path

def changeDomainName(domain):
    newDomain = ""
    if domain == "framework":
        newDomain = "Frameworks"
    elif domain == "library":
        newDomain = "Libraries"
    elif domain == "tool":
        newDomain = "Tools"
    return newDomain



def evaluateOutliersIQR(df, nameOfData):
    platformList = df["platform"].unique()
    for platform in platformList:
        dfAux = df[(df['platform'] == platform)]
        data_full = dfAux[nameOfData].dropna()
        data = data_full.dropna()
        stat, p = stats.shapiro(data)
        print(platform + " - " + nameOfData + ' | Distribution: Statistics=%.3f, p=%.3f' % (stat, p))
        # interpret
        alpha = 0.05
        if p > alpha:
            print(platform + " - " + nameOfData + ' | Distribution looks Gaussian (fail to reject H0)')
        else:
            print(platform + " - " + nameOfData + ' | Distribution does not look Gaussian (reject H0)')
        # calculate interquartile range
        q25, q75 = percentile(data, 25), percentile(data, 75)
        iqr = q75 - q25
        print(platform + " - " + nameOfData + ' | Percentiles: 25th=%.3f, 75th=%.3f, IQR=%.3f' % (q25, q75, iqr))
        # calculate the outlier cutoff
        k = 1.5
        cut_off = iqr * k
        lower, upper = q25 - cut_off, q75 + cut_off
        # identify outliers
        outliers = [x for x in data if x < lower or x > upper]
        outliersPercentage = len(outliers)/data_full.size
        #print(data.size)
        print(platform + " - " + nameOfData + " | Identified outliers: " + str(sorted(outliers)) + " | Outliers observations: %d" % len(outliers) + " out of " + str(data.size) + str("({:.2%})").format(outliersPercentage))
        # remove outliers
        outliers_removed = [x for x in data if x >= lower and x <= upper]
        print(platform + " - " + nameOfData + ' | Non-outlier observations: %d' % len(outliers_removed))
    return df

def formatToLatex(df_described, columnsToLatex, outputPath, fileName):
    fileName += ".latex"
    outputFile = os.path.join(outputPath, fileName)
    f = open(outputFile, "w+")
    for index, row in df_described.iterrows():
        f.write("& & ")
        for key in columnsToLatex:
            value = row.get(key)
            if index != "count" and "/" in key:
                if math.isnan(value):
                    f.write("NaN")
                else:
                    f.write(str("{:.2%}").format(value).replace("%", "\%").replace(".00", ""))
            elif index != "count":
                if math.isnan(value):
                    f.write("NaN")
                else:
                    f.write(str("{:,.2f}").format(value).replace(".00", ""))
            else:
                f.write(str(value).replace(".0", ""))
            f.write(" & ")
        f.write("{\\bf " + index.replace("%", "\%") + "}\\\\ \n")
    f.close()



def generateCSV(df, outputdir, tableName, namePrefix, columnsToLatex):
    tableName += ".csv"
    output_path = createDirs(outputdir)
    df.to_csv(output_path / tableName, index=False, na_rep="NaN", sep=";")
    # df.loc[(df['platform'] == platform)].describe().round(2).to_csv(
    #     output_path / "RQ2_description.csv", index=True,
    #     na_rep="NaN", sep=";")
    platformList = df["platform"].unique()
    domainList = df["domain"].unique()
    for platform in platformList:
        filePath = namePrefix + "_" + platform.replace("/", "#") + "_description"
        df_described = df.loc[(df['platform'] == platform)].describe().round(4)
        filePath += ".csv"
        df_described.to_csv(
            output_path / filePath, index=True,
            na_rep="NaN", sep=";")
        formatToLatex(df_described, columnsToLatex, output_path, filePath)
        for domain in domainList:
            filePath = namePrefix + "_" + platform.replace("/", "#") + "_" + domain + "_description"
            df_described_aux = df.loc[
                (df['platform'] == platform) & (df['domain'] == domain)].describe().round(4)
            filePath += ".csv"
            df_described_aux.to_csv(
                output_path / filePath, index=True,
                na_rep="NaN", sep=";")
            formatToLatex(df_described_aux, columnsToLatex, output_path, filePath)

#https://stackoverflow.com/questions/7015587/python-difference-of-2-datetimes-in-months
def monthdelta(d1, d2):
    delta = 0
    while True:
        mdays = monthrange(d1.year, d1.month)[1]
        d1 += timedelta(days=mdays)
        if d1 <= d2:
            delta += 1
        else:
            break
    return delta

def autolabelHorizontal(rects, ay, ypos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    ypos = ypos.lower()  # normalize the case of the parameter
    va = {'center': 'center', 'top': 'bottom', 'bottom': 'top'}
    offset = {'center': 0.5, 'top': 0.57, 'bottom': 0.43}  # y_txt = y + w*off

    for rect in rects:
        width = rect.get_width()
        ay.text(1.01 * width, rect.get_y() + rect.get_height() * offset[ypos],
                '{}'.format(width), va=va[ypos], ha='left')