import csv
import os
import statistics
from pathlib import Path
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import gridspec
from matplotlib.ticker import (MultipleLocator)
from pymongo import MongoClient
from scipy import stats
from numpy import mean
from numpy import std
from numpy import percentile


from main import Util
from main import StatisticalTests

np.warnings.filterwarnings('ignore')

sns.set_context("paper")
sns.set_style("whitegrid")
# sns.despine()

client = MongoClient('mongodb://localhost:27017/')
db = client['ExceptionHunter']
collection = db['ExceptionDB']


SHOW_FIGURE = False

df_RQ = ""

# SIZE_AXES_LABELS = 12
# SIZE_AXES_VALUES = 10
# SIZE_AXES_TITLE = 12
# SIZE_BAR_VALUES = 10
# SIZE_LEGEND_TITLE = 12
# SIZE_LEGEND_TEXT = 10

SIZE_AXES_LABELS = "large"
SIZE_AXES_VALUES = "medium"
SIZE_AXES_TITLE = "large"
SIZE_BAR_VALUES = "medium"
SIZE_LEGEND_TITLE = "medium"
SIZE_LEGEND_TEXT = "small"

def createDirs(path):
    output_path = Path(path)
    output_path.mkdir(parents=True, exist_ok=True)
    return output_path


# print (sns.color_palette().as_hex())

# COLOR_PALETTE = {
#     "Frameworks": "#1f77b4",
#     "Libraries": "#ff7f0e",
#     "Tools": "#2ca02c",
#     "Desktop/Server": "#53afc9",
#     "Mobile": "#94d1bf",
#     "Multi-platform": "#2ca02c"
# }

# COLOR_PALETTE = {
#     "Frameworks": "#1f77b4",
#     "Libraries": "#ff7f0e",
#     "Tools": "#2ca02c",
#     "Desktop/Server": "#a6cee3",
#     "Mobile": "#ffff99",
#     "Multi-platform": "#fdbf6f"
# }


COLOR_PALETTE = {
    "Frameworks": "#1f77b4",
    "Libraries": "#ff7f0e",
    "Tools": "#2ca02c",
    "Desktop/Server": "#1eae28",
    "Mobile": "#f25e5a",
    "Multi-platform": "#4c88ff"
}

LABELS = ["ZERO", "> ZERO"]
COLOR_DEGRADE = {
    LABELS[0]: "#abd0e6",
    LABELS[1]: "#1b69af",
}

FIGURE_ORDER = ["Desktop/Server", "Mobile", "Multi-platform"]

# print(sns.color_palette("GnBu_d").as_hex())
print(sns.color_palette("Paired").as_hex())

output_figures_path = createDirs("RQs/RQ1/figures/")


############################## TABLE GENERATION ##############################
def formatToLatex(df_described, outputPath, fileName):
    keyList = ["NEBTM", "NTM", "NEBTM/NTM", "NDUE", "NDTE", "NDTE/NDUE"]
    fileName += ".latex"
    outputFile = os.path.join(outputPath, fileName)
    f = open(outputFile, "w+")
    for index, row in df_described.iterrows():
        if index == "count":
            continue
        f.write("& & ")
        for key in keyList:
            value = row.get(key)
            if "/" in key:
                # value = row.get(key)
                f.write(str("{:.2%}").format(value).replace("%", "\%").replace(".00", ""))
            else:
                f.write(str("{:,.2f}").format(value).replace(".00", ""))
            f.write(" & ")
        f.write("{\\bf " + index.replace("%", "\%") + "}\\\\ \n")
    f.close()



def generateCSV():
    output_path = createDirs("RQs/RQ1/")
    df_RQ.to_csv(output_path / 'tableIII.csv', index=False, na_rep="NaN", sep=";")
    # df_RQ.loc[(df_RQ['platform'] == platform)].describe().round(2).to_csv(
    #     output_path / "RQ1_description.csv", index=True,
    #     na_rep="NaN", sep=";")
    platformList = df_RQ["platform"].unique()
    domainList = df_RQ["domain"].unique()
    for platform in platformList:
        filePath = "RQ1_" + platform.replace("/", "#") + "_description"
        df_described = df_RQ.loc[(df_RQ['platform'] == platform)].describe().round(4)
        filePath += ".csv"
        df_described.to_csv(
            output_path / filePath, index=True,
            na_rep="NaN", sep=";")
        formatToLatex(df_described, output_path, filePath)
        for domain in domainList:
            filePath = "RQ1_" + platform.replace("/", "#") + "_" + domain + "_description"
            df_described_aux = df_RQ.loc[
                (df_RQ['platform'] == platform) & (df_RQ['domain'] == domain)].describe().round(4)
            filePath += ".csv"
            df_described_aux.to_csv(
                output_path / filePath, index=True,
                na_rep="NaN", sep=";")
            formatToLatex(df_described_aux, output_path, filePath)

def changeDomainName(domain):
    newDomain = ""
    if domain == "framework":
        newDomain = "Frameworks"
    elif domain == "library":
        newDomain = "Libraries"
    elif domain == "tool":
        newDomain = "Tools"
    return newDomain


def generateDataSetRQ1():

    global df_RQ
    today = datetime.now()
    numberOfProjectsWithoutExceptionsUse = 0
    numberOfProjectsWithoutTests = 0
    numberOfProjectsWithLesssJUnitTestsThanOther = 0
    numberOfProjectsmultiplatform = 0
    dataSet = []
    for doc in collection.find():
        if doc['projectDetails']["platform"] == "Multi-platform":
            numberOfProjectsmultiplatform += 1
            #continue

        if doc['statistics']["totalNumberOfTestMethods"] == 0:
            numberOfProjectsWithoutTests += 1
            #continue

        # if doc["statistics"]['totalNumberOfJUnitTests'] == 0:
        #     numberOfProjectsWithoutTests += 1
        #     continue

        # if doc["statistics"]['totalNumberOfJUnitTests'] < doc["statistics"]['totalNumberOfTestNGTests']  or doc["statistics"]['totalNumberOfJUnitTests'] < doc["statistics"]['totalNumberOfNotIdentifiedTests']:
        #     numberOfProjectsWithLesssJUnitTestsThanOther += 1
        #     continue


        if doc["statistics"]['totalNumberOfCustomExceptionsUses'] == 0 and doc["statistics"][
            'totalNumberOfStandardOrThirdPartyExceptionsUses'] == 0:
            numberOfProjectsWithoutExceptionsUse += 1
            #continue

        project = doc["projectName"]
        tagCreatedOn = datetime.strptime(doc["tagCreatedAt"], '%d/%m/%Y %H:%M:%S').year
        projectCreatedOn = datetime.strptime(doc["projectDetails"]["createdAt"], '%d/%m/%Y %H:%M:%S').year
        projectMonths = Util.monthdelta(datetime.strptime(doc["projectDetails"]["createdAt"], '%d/%m/%Y %H:%M:%S'), today)
        platform = doc['projectDetails']["platform"]
        domain = changeDomainName(doc['projectDetails']["domain"])
        description = "'" + doc['projectDetails']["description"] + "'"
        gitRepo = doc['projectDetails']["gitRepo"]
        stars = doc['projectDetails']["stars"]
        contributors = doc['projectDetails']["contributors"]
        NEBTM = doc["statistics"]['totalNumberOfExceptionalBehaviorTestMethods']
        NTM = doc["statistics"]['totalNumberOfTestMethods']
        NEBTM_NTM = round(np.float64(NEBTM) / np.float64(NTM), 4)
        NDTE = doc["statistics"]['totalNumberOfAllTestedExceptions']
        NDTUE = doc["statistics"]['totalNumberOfDistinctTestedExceptions']  # exceções usadas e testadas
        NDUE = doc["statistics"]['totalNumberOfDistinctUsedExceptions']  # numero de exceções distintas usadas
        NEU = doc["statistics"]['totalNumberOfExceptionsUses']  # numero de pontos de usos de exceções
        NDTE_NDUE = round(np.float64(NDTE) / np.float64(NDUE), 4)
        NDTUE_NDUE = round(np.float64(NDTUE) / np.float64(NDUE), 4)

        dataSetRQ1 = [project, description, gitRepo, tagCreatedOn, projectCreatedOn, projectMonths, platform, domain, stars,
                      contributors, NEBTM, NTM,
                      NEBTM_NTM, NDTE, NDTUE, NDUE, NEU, NDTE_NDUE, NDTUE_NDUE]
        dataSet.append(dataSetRQ1)

    df_RQ = pd.DataFrame(dataSet,
                         columns=['project', 'description', 'gitRepo', 'tagCreatedOn', 'projectCreatedOn', 'projectMonths', 'platform', 'domain', 'stars',
                                   'contributors', 'NEBTM', 'NTM', 'NEBTM/NTM', 'NDTE', 'NDTUE', 'NDUE', 'NEU',
                                   'NDTE/NDUE',
                                   'NDTUE/NDUE'])
    df_RQ['log10(NTM)'] = np.log10(df_RQ['NTM'] + 1)
    df_RQ['log10(NEU)'] = np.log10(df_RQ['NEU'] + 1)
    df_RQ['log10(NEBTM)'] = np.log10(df_RQ['NEBTM'] + 1)
    df_RQ['log10(NTM-NEBTM)'] = np.log10(df_RQ['NTM'] + 1 - df_RQ['NEBTM'] + 1)
    #df_RQ['NTM-NEBTM'] = df_RQ['NTM'] + 1 - df_RQ['NEBTM'] + 1
    df_RQ['NTM-NEBTM'] = df_RQ['NTM'] - df_RQ['NEBTM']
    df_RQ['log10(stars)'] = np.log10(df_RQ['stars'])
    print("numberOfProjectsMulti-platform: " + str(numberOfProjectsmultiplatform))
    print("numberOfProjectsWithoutExceptionsUse: " + str(numberOfProjectsWithoutExceptionsUse))
    print("numberOfProjectsWithoutTests: " + str(numberOfProjectsWithoutTests))
    print("numberOfProjectsWithLesssJUnitTestsThanOther: " + str(numberOfProjectsWithLesssJUnitTestsThanOther))
    totalNumberOfTestMethods = df_RQ['NTM'].sum()
    print("totalNumberOfTestMethods: " + str(totalNumberOfTestMethods))
    generateCSV()


############################## RQ1 - PART 1 ##############################
def change_width(ax, new_value):
    for patch in ax.patches:
        current_width = patch.get_width()
        diff = current_width - new_value
        # we change the bar width
        patch.set_width(new_value)
        # we recenter the bar
        patch.set_x(patch.get_x() + diff * .5)
        return ax


def visaoGeralDF(printAll=False):
    # by platform
    ax = sns.pairplot(data=df_RQ,
                      vars=['projectCreatedAt', 'tagCreatedAt', 'stars', 'contributors', 'NEBTM/NTM', 'NDTE/NDUE',
                            'NDTUE/NDUE'], hue="platform", palette=COLOR_PALETTE)
    # ax = sns.pairplot(data=df_RQ, hue="platform", palette=COLOR_PALETTE)
    plt.savefig(output_figures_path / 'VisaoGeralDF_PLATFORM.png', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()
    if (printAll):
        # by domain
        ax = sns.pairplot(data=df_RQ, hue="domain", palette=COLOR_PALETTE)
        plt.savefig(output_figures_path / 'VisaoGeralDF_DOMAIN.png', bbox_inches="tight")
        if (SHOW_FIGURE):
            plt.show()
        plt.clf()
        # only domain Mobile by platform
        df_mobile = df_RQ.loc[df_RQ['platform'] == "Mobile"]
        ax = sns.pairplot(data=df_mobile, hue="domain", palette=COLOR_PALETTE)
        plt.savefig(output_figures_path / 'VisaoGeraldf_mobile_DOMAIN.png', bbox_inches="tight")
        if (SHOW_FIGURE):
            plt.show()
        plt.clf()
        # only domain Desktop/Server by platform
        df_desktopServers = df_RQ.loc[df_RQ['platform'] == 'Desktop/Server']
        ax = sns.pairplot(data=df_desktopServers, hue="domain", palette=COLOR_PALETTE)
        plt.savefig(output_figures_path / 'VisaoGeralDF_DESKTOP-SERVERS_DOMAIN.png', bbox_inches="tight")
        if (SHOW_FIGURE):
            plt.show()
        plt.clf()
        # only domain Desktop/Server by platform
        df_desktopServers = df_RQ.loc[df_RQ['platform'] == 'Multi-platform']
        ax = sns.pairplot(data=df_desktopServers, hue="domain", palette=COLOR_PALETTE)
        plt.savefig(output_figures_path / 'VisaoGeralDF_Multi-platform_DOMAIN.png', bbox_inches="tight")
        if (SHOW_FIGURE):
            plt.show()
        plt.clf()


def corrfunc(x, y, **kws):
    if kws['label'] == 'other':
        r_label = 'rO'
        pos_x = 0
    elif kws['label'] == 'Libraries':
        r_label = 'rL'
        pos_x = 0.3
    elif kws['label'] == 'Frameworks':
        r_label = 'rF'
        pos_x = 0.6
    elif kws['label'] == 'Tools':
        r_label = 'rF'
        pos_x = 0.9
    # r, p = stats.spearmanr(x, y)
    r, p = stats.spearmanr(x, y)
    p_stars = ''
    if p <= 0.05:
        p_stars = '*'
    if p <= 0.01:
        p_stars = '**'
    if p <= 0.001:
        p_stars = '***'
    ax = plt.gca()
    ax.annotate(r_label + ' = {:.2f}'.format(r) + p_stars,
                xy=(pos_x, 1.015), xycoords=ax.transAxes, fontweight='bold')


# https://stackoverflow.com/questions/48139899/correlation-matrix-plot-with-coefficients-on-one-side-scatterplots-on-another
def correlationRQ1_pairplot():
    for platform in df_RQ['platform'].unique():
        df_desktopServers = df_RQ.loc[df_RQ['platform'] == platform]
        g = sns.pairplot(data=df_desktopServers,
                         x_vars=['log10(NEBTM)', 'NEBTM/NTM', 'NDTE/NDUE',
                                 'NDTUE/NDUE'],
                         y_vars=['projectCreatedOn', 'tagCreatedOn', 'log10(stars)', 'contributors', 'log10(NEU)',
                                 'log10(NTM)'], hue='domain', palette=COLOR_PALETTE)
        g.map(corrfunc)
        g.fig.set_figheight(16)
        g.fig.set_figwidth(12)
        g.fig.suptitle(platform, fontsize='xx-large', y=1.00)  # y= some height>1

        # for i in range(len(g.fig.get_children()[-1].texts)):
        #     label = g.fig.get_children()[-1].texts[i].get_text()
        #     if label == 'other':
        #         replacement = 'other (rO)'
        #     elif label == 'Libraries':
        #         replacement = 'Libraries (rL)'
        #     elif label == 'Frameworks':
        #         replacement = 'Frameworks (rF)'
        #     g.fig.get_children()[-1].texts[i].set_text(replacement)
        #
        # g._legend.set_bbox_to_anchor((1.008, 0.55))

        g._legend.remove()

        handles = g._legend_data.values()
        labels = list(g._legend_data.keys())
        for i in range(len(labels)):
            label = labels[i]
            if label == 'other':
                replacement = 'other (rO)'
            elif label == 'Libraries':
                replacement = 'Libraries (rL)'
            elif label == 'Frameworks':
                replacement = 'Frameworks (rF)'
            labels[i] = replacement

        g.fig.legend(handles=handles, labels=labels, loc='upper center', bbox_to_anchor=(0.5, 0.99), ncol=3,
                     fontsize="large", frameon=True)
        g.fig.subplots_adjust(top=0.95)

        for ax in g.axes.flat:
            # ax.get_yaxis().set_tick_params(labelsize='x-large', which="major")
            # labels ao redor dos graficos
            # ax.set_xlabel(ax.get_xlabel(), fontsize='20')
            ax.set_xlabel(ax.get_xlabel(), fontsize=SIZE_AXES_LABELS)
            y_label = ax.get_ylabel()
            if y_label == 'projectCreatedOn':
                y_label = 'project created on'
            elif y_label == 'tagCreatedOn':
                y_label = 'tag created on'
            ax.set_ylabel(y_label, fontsize=SIZE_AXES_LABELS)
            ax.yaxis.grid(True, linewidth=1, which="major")
            if ax.rowNum > 1:
                # ax.xaxis.set_major_formatter(ticker.EngFormatter())
                ax.yaxis.set_major_formatter(ticker.EngFormatter())
            ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_TITLE, which="major")
            ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_TITLE)

        figure_name = 'VisaoGeralDF_' + platform.replace("/", "-") + '.png'
        plt.savefig(output_figures_path / figure_name)
        if (SHOW_FIGURE):
            plt.show()
        plt.clf()


def correlationRQ1_heatmap():
    # by platform
    ax = sns.pairplot(data=df_RQ,
                      x_vars=['NEBTM', 'NEBTM/NTM', 'NDTE/NDUE',
                              'NDTUE/NDUE'],
                      y_vars=['projectCreatedOn', 'tagCreatedOn', 'stars', 'contributors', 'NEU'],
                      hue="platform", palette=COLOR_PALETTE)
    sns.heatmap(df_RQ.corr(), annot=True, fmt=".2f")
    plt.savefig(output_figures_path / 'VisaoGeralDF_PLATFORM.png')
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def pairPlotProjectDates():
    bins = np.arange(2007, 2020, 1)
    g = sns.FacetGrid(df_RQ, col="platform", sharex=True, sharey=True, hue="platform", palette=COLOR_PALETTE, height=3,
                      aspect=1.5)
    g = (g.map(sns.distplot, "projectCreatedOn", kde=False, rug=False, bins=bins)).set(
        xticks=[2007, 2010, 2013, 2016, 2019], yticks=[0, 10, 20, 30])
    g.axes[0, 0].get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    g.axes[0, 0].get_xaxis().set_tick_params(labelsize=SIZE_AXES_VALUES)
    g.axes[0, 1].get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    g.axes[0, 1].get_xaxis().set_tick_params(labelsize=SIZE_AXES_VALUES)

    g.fig.subplots_adjust(wspace=.05, hspace=.05)

    for ax in g.axes.flat:
        # ax.get_yaxis().set_tick_params(labelsize='x-large', which="major")
        # labels ao redor dos graficos
        # ax.set_xlabel(ax.get_xlabel(), fontsize='20')
        ax.set_xlabel("", fontsize=SIZE_AXES_LABELS)
        ax.set_ylabel(ax.get_ylabel(), fontsize=SIZE_AXES_LABELS)

        # títulos em cima dos graficos
        if ax.get_title():
            ax.set_title(ax.get_title().split('=')[1],
                         fontsize=SIZE_AXES_TITLE)

        # ax.set(xlabel='', ylabel='', title='')
        # plt.setp(ax.get_legend().get_texts(), fontsize='18')  # for legend text
        # plt.setp(ax.get_legend().get_title(), fontsize='20')  # for legend title
        ax.yaxis.grid(True, linewidth=1, which="major")
        ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_TITLE, which="major")
        ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_TITLE)
    plt.tight_layout()
    plt.savefig(output_figures_path / 'pairPlotProjectDates.png', bbox_inches="tight")
    plt.savefig(output_figures_path / 'pairPlotProjectDates.pdf', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()





def violinPlot_ProjectCreatedOn():
    fig, ax = plt.subplots(figsize=(6, 7))
    ax = sns.violinplot(x="domain", y="projectCreatedOn", data=df_RQ, cut=0, hue="platform", inner="box",
                        scale="count", palette=COLOR_PALETTE, linewidth=1.5, ax=ax)
    ax.set(xlabel='', ylabel='', title='')
    ax.legend(title="platform", loc="lower center")
    ax.yaxis.grid(True, linewidth=1, which="major")
    plt.setp(ax.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title
    ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax.xaxis.set_ticks_position('bottom')
    # Show graphic
    plt.tight_layout()
    plt.savefig(output_figures_path / 'violinPlot_ProjectCreatedOn.pdf', bbox_inches="tight")
    plt.savefig(output_figures_path / 'violinPlot_ProjectCreatedOn.png', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def violinPlot_TagCreatedOn():
    fig, ax = plt.subplots(figsize=(6, 7))
    ax = sns.violinplot(x="domain", y="tagCreatedOn", data=df_RQ, cut=0, hue="platform", inner="box", scale="count",
                        palette=COLOR_PALETTE, linewidth=1.5, ax=ax)
    ax.set(xlabel='', ylabel='', title='')
    ax.legend(title="platform", loc="lower center")
    ax.yaxis.grid(True, linewidth=1, which="major")
    plt.setp(ax.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title
    ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax.xaxis.set_ticks_position('bottom')
    # Show graphic
    plt.tight_layout()
    plt.savefig(output_figures_path / 'violinPlot_TagCreatedOn.pdf', bbox_inches="tight")
    plt.savefig(output_figures_path / 'violinPlot_TagCreatedOn.png', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def violinPlotRatio_Contributors():
    fig, ax = plt.subplots(figsize=(6, 7))
    ax = sns.violinplot(x="domain", y="contributors", data=df_RQ, cut=0, hue="platform", inner="box", scale="count",
                        palette=COLOR_PALETTE, linewidth=1.5, ax=ax)
    # ax = sns.swarmplot(x="domain", y="contributors", data=df_RQ, hue="platform")
    ax.set(xlabel='', ylabel='', title='')
    ax.legend(title="platform", loc="upper center")
    ax.yaxis.grid(True, linewidth=1, which="major")
    plt.setp(ax.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title
    ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax.xaxis.set_ticks_position('bottom')
    # Show graphic
    plt.tight_layout()
    plt.savefig(output_figures_path / 'violinPlot_Contributors.pdf', bbox_inches="tight")
    plt.savefig(output_figures_path / 'violinPlot_Contributors.png', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def violinPlotRatio_Stars():
    fig, ax = plt.subplots(figsize=(6, 7))
    # ax.set(yscale="log")
    # ax = sns.swarmplot(x="domain", y="stars", data=df_RQ, hue="platform")
    ax = sns.violinplot(x="domain", y="stars", data=df_RQ, cut=0, hue="platform", inner="box", scale="count",
                        palette=COLOR_PALETTE, linewidth=1.5, ax=ax)
    ax.set(xlabel='', ylabel='', title='')
    ax.set_ylabel(ax.get_ylabel(), fontsize=SIZE_AXES_LABELS)
    ax.set_yticks([3000, 5000, 10000, 20000, 30000, 40000, 50000])
    ax.yaxis.grid(True, linewidth=1, which="major")
    plt.setp(ax.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title
    # ax.set_yscale('log')
    ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_major_formatter(ticker.EngFormatter())
    # Show graphic
    plt.tight_layout()
    plt.savefig(output_figures_path / 'violinPlotRatio_Stars.pdf', bbox_inches="tight")
    plt.savefig(output_figures_path / 'violinPlotRatio_Stars.png', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def numberOfTestMethodsPerProject_histogram():
    bins = np.arange(0, 6, 0.5)
    g = sns.FacetGrid(df_RQ, col="platform", sharex=False, hue="platform", palette=COLOR_PALETTE)
    g = (g.map(sns.distplot, "log10(NTM)", kde=False, rug=True, bins=bins)).set(xticks=[0, 1, 2, 3, 4, 5],
                                                                                yticks=[10, 20, 30, 40])
    for ax in g.axes.flat:
        # ax.get_yaxis().set_tick_params(labelsize='x-large', which="major")
        # labels ao redor dos graficos
        # ax.set_xlabel(ax.get_xlabel(), fontsize='20')
        ax.set_xlabel(ax.get_xlabel(), fontsize=SIZE_AXES_LABELS)
        ax.set_ylabel(ax.get_ylabel(), fontsize=SIZE_AXES_LABELS)

        # títulos em cima dos graficos
        if ax.get_title():
            ax.set_title(ax.get_title().split('=')[1],
                         fontsize=SIZE_AXES_TITLE)

        ax.yaxis.grid(True, linewidth=1, which="major")
        ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_TITLE, which="major")
        ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_TITLE)
    plt.tight_layout()
    plt.savefig(output_figures_path / 'RQ1_Fig1_numberOfTestMethodsPerProject_bar.png',
                bbox_inches="tight")
    plt.savefig(output_figures_path / 'RQ1_Fig1_numberOfTestMethodsPerProject_bar.pdf', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def numberOfExceptionalBehaviorTestMethodsPerProject_histogram():
    bins = np.arange(0, 5, 0.5)
    g = sns.FacetGrid(df_RQ, col="platform", sharex=False, hue="platform", palette=COLOR_PALETTE)
    g = (g.map(sns.distplot, "log10(NEBTM)", kde=False, rug=True, bins=bins)).set(xticks=[0, 1, 2, 3, 4],
                                                                                  yticks=[10, 20, 30, 40, 50])

    for ax in g.axes.flat:
        # ax.get_yaxis().set_tick_params(labelsize='x-large', which="major")
        # labels ao redor dos graficos
        # ax.set_xlabel(ax.get_xlabel(), fontsize='20')
        ax.set_xlabel(ax.get_xlabel(), fontsize=SIZE_AXES_LABELS)
        ax.set_ylabel(ax.get_ylabel(), fontsize=SIZE_AXES_LABELS)

        # títulos em cima dos graficos
        if ax.get_title():
            ax.set_title(ax.get_title().split('=')[1],
                         fontsize=SIZE_AXES_TITLE)

        ax.yaxis.grid(True, linewidth=1, which="major")
        ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_TITLE, which="major")
        ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_TITLE)

    plt.tight_layout()
    plt.savefig(output_figures_path / 'RQ1_Fig2_numberOfExceptionalBehaviorExceptionTestMethodsPerProject_bar.png',
                bbox_inches="tight")
    plt.savefig(output_figures_path / 'RQ1_Fig2_numberOfExceptionalBehaviorExceptionTestMethodsPerProject_bar.pdf',
                bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfTestMethods_scatterplot():
    bins = np.arange(0, 5, 0.5)
    g = sns.FacetGrid(df_RQ, col="platform", sharex=False, hue="domain", palette=COLOR_PALETTE)
    g = (g.map(plt.scatter, "log10(NTM)",
               "log10(NEBTM)")).add_legend()  # .set(xticks=[0, 1, 2, 3, 4], yticks=[0, 20, 40, 60])
    g.axes[0, 0].get_yaxis().set_tick_params(labelsize=14, which="major")
    # g.axes[0, 1].get_yaxis().set_tick_params(labelsize=14, which="major") #TODO retirar isso aqui
    # g.map(corrfunc)

    # plt.tight_layout()
    plt.savefig(
        output_figures_path / 'Fig1_numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfTestMethods_scatterplot.png',
        bbox_inches="tight")
    # plt.savefig(output_figures_path / 'Fig1_numberOfExceptionalBehaviorTestMethodsPerProject_bar.pdf', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfTestMethods_linearregression():
    df_mobile = df_RQ[df_RQ['platform'] == "Mobile"]
    # df_mobile = df_mobile[['log10(NTM)', 'log10(NEBTM)']]
    mobile_correlation, mobile_p_value = stats.spearmanr(df_mobile['NTM-NEBTM'], df_mobile['NEBTM'])

    df_desktopservers = df_RQ[df_RQ['platform'] == 'Desktop/Server']
    desktopservers_correlation, desktopservers_p_value = stats.spearmanr(df_desktopservers['NTM-NEBTM'],
                                                                         df_desktopservers['NEBTM'])

    df_multiplatform = df_RQ[df_RQ['platform'] == 'Multi-platform']
    multiplatform_correlation, multiplatform_p_value = stats.spearmanr(df_multiplatform['NTM-NEBTM'],
                                                                         df_multiplatform['NEBTM'])
    # df_desktopservers = df_desktopservers[['log10(NTM)', 'log10(NEBTM)']]
    print("Mobile Correlation: " + str(mobile_correlation) + " pvalue: " + str(mobile_p_value))
    print("Desktop/Server Correlation: " + str(desktopservers_correlation) + " pvalue: " + str(desktopservers_p_value))
    print("Multi-platform Correlation: " + str(multiplatform_correlation) + " pvalue: " + str(multiplatform_p_value))
    # print("Mobile correlation: " + df_mobile.corr())
    # print("Desktop/Server correlation: " + df_desktopservers.corr())

    g = sns.lmplot(y="log10(NTM-NEBTM)", x="log10(NEBTM)", hue="domain", col="platform",
                   col_order=FIGURE_ORDER, palette=COLOR_PALETTE,
                   data=df_RQ, legend=False).set(xticks=[0, 1, 2, 3, 4, 5], yticks=[0, 1, 2, 3, 4, 5, 6])
    # .set(yticks=[-1, 0, 1, 2, 3, 4, 5], xticks=[-2, -1, 0, 1, 2, 3, 4, 5])

    # Legend
    ax_aux = g.axes[0, 1]
    ax_aux.legend(title="", loc="upper left")
    plt.setp(ax_aux.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax_aux.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title

    for ax in g.axes.flat:
        ax.set_xlim(-1, 5)
        ax.set_ylim(-1, 6)
        # ax.get_yaxis().set_tick_params(labelsize='x-large', which="major")
        # labels ao redor dos graficos
        # ax.set_xlabel(ax.get_xlabel(), fontsize='20')
        ax.set_xlabel(ax.get_xlabel(), fontsize=SIZE_AXES_LABELS)
        ax.set_ylabel(ax.get_ylabel(), fontsize=SIZE_AXES_LABELS)

        # títulos em cima dos graficos
        if ax.get_title():
            ax.set_title(ax.get_title().split('=')[1],
                         fontsize=SIZE_AXES_TITLE)

        ax.yaxis.grid(True, linewidth=1, which="major")
        ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_TITLE, which="major")
        ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_TITLE)

    # plt.tight_layout()
    plt.savefig(
        output_figures_path / 'G1-Q1-Fig2_numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfTestMethods_regressao.png',
        bbox_inches="tight")
    plt.savefig(
        output_figures_path / 'G1-Q1-Fig2_numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfTestMethods_regressao.pdf',
        bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfExceptionsUses_linearregression():
    df_mobile = df_RQ[df_RQ['platform'] == 'Mobile']
    # df_mobile = df_mobile[['log10(NTM)', 'log10(NEBTM)']]
    mobile_correlation, mobile_p_value = stats.spearmanr(df_mobile['log10(NEU)'], df_mobile['log10(NEBTM)'])
    df_desktopservers = df_RQ[df_RQ['platform'] == 'Desktop/Server']
    desktopservers_correlation, desktopservers_p_value = stats.spearmanr(df_desktopservers['log10(NEU)'],
                                                                         df_desktopservers['log10(NEBTM)'])
    df_multiplatform = df_RQ[df_RQ['platform'] == 'Multi-platform']
    multiplatform_correlation, multiplatform_p_value = stats.spearmanr(df_multiplatform['log10(NEU)'],
                                                                         df_multiplatform['log10(NEBTM)'])
    # df_desktopservers = df_desktopservers[['log10(NTM)', 'log10(NEBTM)']]
    print("Mobile Correlation (exception uses x exception tests): " + str(mobile_correlation) + " pvalue: " + str(
        mobile_p_value))
    print("Desktop/Server Correlation (exception uses x exception tests): " + str(
        desktopservers_correlation) + " pvalue: " + str(desktopservers_p_value))
    print("Multi-platform Correlation (exception uses x exception tests): " + str(
        desktopservers_correlation) + " pvalue: " + str(multiplatform_p_value))
    # print("Mobile correlation: " + df_mobile.corr())
    # print("Desktop/Server correlation: " + df_desktopservers.corr())
    g = sns.lmplot(x="log10(NEU)", y="log10(NEBTM)", hue="domain", col="platform", data=df_RQ, legend=False).set(
        xticks=[-1, 0, 1, 2, 3, 4, 5], yticks=[-4, -3, -2, -1, 0, 1, 2, 3, 4, 5])

    # Legend
    ax_aux = g.axes[0, 1]
    ax_aux.legend(title="domain", loc="upper left")
    plt.setp(ax_aux.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax_aux.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title

    for ax in g.axes.flat:
        # ax.get_yaxis().set_tick_params(labelsize='x-large', which="major")
        # labels ao redor dos graficos
        # ax.set_xlabel(ax.get_xlabel(), fontsize='20')
        ax.set_xlabel(ax.get_xlabel(), fontsize=SIZE_AXES_LABELS)
        ax.set_ylabel(ax.get_ylabel(), fontsize=SIZE_AXES_LABELS)

        # títulos em cima dos graficos
        if ax.get_title():
            ax.set_title(ax.get_title().split('=')[1],
                         fontsize=SIZE_AXES_TITLE)

        ax.yaxis.grid(True, linewidth=1, which="major")
        ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_TITLE, which="major")
        ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_TITLE)

    # plt.tight_layout()
    plt.savefig(
        output_figures_path / 'Fig1_numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfExceptionsUses_regressao.png',
        bbox_inches="tight")
    # plt.savefig(output_figures_path / 'Fig1_numberOfExceptionalBehaviorTestMethodsPerProject_bar.pdf', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()



def calculateCorrelations():
    print("************* CORRELATIONS ************")
    StatisticalTests.evaluateCorrelation(df_RQ, "NTM-NEBTM", "NEBTM", False)
    StatisticalTests.evaluateCorrelation(df_RQ, "NTM-NEBTM", "NEBTM", False)
    StatisticalTests.evaluateCorrelation(df_RQ, "projectMonths", "NEBTM", False)
    StatisticalTests.evaluateCorrelation(df_RQ, "contributors", "NEBTM", False)
    StatisticalTests.evaluateCorrelation(df_RQ, "projectMonths", "NEBTM", False)
    StatisticalTests.evaluateCorrelation(df_RQ, "contributors", "NEBTM", False)
    StatisticalTests.evaluateCorrelation(df_RQ, "projectMonths", "NEBTM/NTM", False )
    StatisticalTests.evaluateCorrelation(df_RQ, "contributors", "NEBTM/NTM", False)
    StatisticalTests.evaluateCorrelation(df_RQ, "projectMonths", "NDTUE/NDUE", False )
    StatisticalTests.evaluateCorrelation(df_RQ, "contributors", "NDTUE/NDUE", False)
    # StatisticalTests.evaluateCorrelation(df_RQ, "NEU", "NEBTM", False)
    # StatisticalTests.evaluateCorrelation(df_RQ, "NEU", "NEBTM/NTM", False)
    # StatisticalTests.evaluateCorrelation(df_RQ, "NEU", "NDTUE/NDUE", False)
    print("************* CORRELATIONS ************")

def numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfContributors_linearregression():
    df_mobile = df_RQ[df_RQ['platform'] == "Mobile"]
    # df_mobile = df_mobile[['log10(NTM)', 'log10(NEBTM)']]
    mobile_correlation, mobile_p_value = stats.spearmanr(df_mobile['contributors'], df_mobile['log10(NEBTM)'])
    df_desktopservers = df_RQ[df_RQ['platform'] == 'Desktop/Server']
    desktopservers_correlation, desktopservers_p_value = stats.spearmanr(df_desktopservers['contributors'],
                                                                         df_desktopservers['log10(NEBTM)'])
    # df_desktopservers = df_desktopservers[['log10(NTM)', 'log10(NEBTM)']]
    print("Mobile Correlation (contributors x exception tests): " + str(mobile_correlation) + " pvalue: " + str(
        mobile_p_value))
    print("Desktop/Server Correlation (contributors x exception tests): " + str(
        desktopservers_correlation) + " pvalue: " + str(desktopservers_p_value))
    # print("Mobile correlation: " + df_mobile.corr())
    # print("Desktop/Server correlation: " + df_desktopservers.corr())
    g = sns.lmplot(x="contributors", y="log10(NEBTM)", hue="domain", col="platform", data=df_RQ, legend=False).set(
        yticks=[-4, -3, -2, -1, 0, 1, 2, 3, 4, 5])

    # Legend
    ax_aux = g.axes[0, 1]
    ax_aux.legend(title="domain", loc="upper left")
    plt.setp(ax_aux.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax_aux.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title

    for ax in g.axes.flat:
        # ax.get_yaxis().set_tick_params(labelsize='x-large', which="major")
        # labels ao redor dos graficos
        # ax.set_xlabel(ax.get_xlabel(), fontsize='20')
        ax.set_xlabel(ax.get_xlabel(), fontsize=SIZE_AXES_LABELS)
        ax.set_ylabel(ax.get_ylabel(), fontsize=SIZE_AXES_LABELS)

        # títulos em cima dos graficos
        if ax.get_title():
            ax.set_title(ax.get_title().split('=')[1],
                         fontsize=SIZE_AXES_TITLE)

        ax.yaxis.grid(True, linewidth=1, which="major")
        ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_TITLE, which="major")
        ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_TITLE)

    # plt.tight_layout()
    plt.savefig(
        output_figures_path / 'Fig1_numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfContributorss_regressao.png',
        bbox_inches="tight")
    # plt.savefig(output_figures_path / 'Fig1_numberOfExceptionalBehaviorTestMethodsPerProject_bar.pdf', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def numberOfExceptionalBehaviorTestMethodsPerProject_violinPlot():
    fig, ax = plt.subplots(figsize=(6, 7))
    ax = sns.violinplot(x="domain", y="log10(NEBTM)", data=df_RQ, cut=0, hue="platform", inner="box", scale="count",
                        palette=COLOR_PALETTE, ax=ax)
    sns.set_style("whitegrid")
    sns.despine()

    # ax = sns.swarmplot(x="domain", y="contributors", data=df_RQ, hue="platform")
    ax.set(xlabel='', ylabel='', title='')
    ax.legend(loc="upper center")
    ax.yaxis.grid(True, linewidth=1, which="major")
    plt.setp(ax.get_legend().get_texts(), fontsize='18')  # for legend text
    plt.setp(ax.get_legend().get_title(), fontsize='20')  # for legend title
    ax.get_yaxis().set_tick_params(labelsize=25, which="major")
    ax.get_xaxis().set_tick_params(direction='out', labelsize=25)
    ax.xaxis.set_ticks_position('bottom')
    plt.tight_layout()
    plt.savefig(output_figures_path / 'Fig1_numberOfExceptionalBehaviorExceptionTestMethodsPerProject_violinplot.png',
                bbox_inches="tight")
    plt.savefig(output_figures_path / 'Fig1_numberOfExceptionalBehaviorExceptionTestMethodsPerProject_violinplot.pdf',
                bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def barPlotNumberOfExceptionalTests_barplot():
    platformList = df_RQ["platform"].unique()
    domainList = df_RQ["domain"].unique()

    df_aux = []
    df_filtered_one_or_more = df_RQ[(df_RQ['NEBTM'] >= 1)]
    totalDfSize = len(df_RQ)
    totalHigherThanZero = len(df_filtered_one_or_more)
    ratioAux = totalHigherThanZero / totalDfSize
    print(f'Total_NEBTM > 0: {totalHigherThanZero} out of {totalDfSize}({ratioAux})')
    for platform in platformList:
        df_platform = df_RQ[(df_RQ['platform'] == platform)]
        df_filtered_one_or_more_platform = df_platform[(df_platform['NEBTM'] >= 1)]
        totalDfSize = len(df_platform)
        totalHigherThanZero = len(df_filtered_one_or_more_platform)
        ratioAux = totalHigherThanZero/totalDfSize
        print(f'{platform}_NEBTM > 0: {totalHigherThanZero} out of {totalDfSize}({ratioAux})')
        for domain in domainList:
            df_domain = df_platform[(df_platform["domain"] == domain)]
            df_filtered_zero_domain = df_domain[(df_domain['NEBTM'] == 0)]
            df_filtered_one_or_more_domain = df_domain[(df_domain['NEBTM'] >= 1)]
            df_aux.append([platform, domain, LABELS[0], len(df_filtered_zero_domain)])
            df_aux.append([platform, domain, LABELS[1], len(df_filtered_one_or_more_domain)])
            totalDfSize = len(df_domain)
            totalHigherThanZero = len(df_filtered_one_or_more_domain)
            ratioAux = totalHigherThanZero / totalDfSize
            print(f'{platform}_{domain}_NEBTM > 0: {totalHigherThanZero} out of {totalDfSize}({ratioAux})')

    df_counter = pd.DataFrame(df_aux, columns=["platform", "domain", "NEBTM", "count"])
    g = sns.catplot(x="domain", y="count", col="platform", hue="NEBTM", data=df_counter, kind="bar",
                    palette=COLOR_DEGRADE, legend=False, legend_out=False, col_order=FIGURE_ORDER)
    g.fig.subplots_adjust(wspace=.05, hspace=.05)
    g.fig.set_figheight(3)
    g.fig.set_figwidth(6)

    # Legend
    ax_aux = g.axes[0, 1]
    ax_aux.legend(title="NEBTM", loc="upper center")
    #ax_aux.legend(title="NEBTM", loc="upper center", bbox_to_anchor=(0.5, 1.20), ncol=2, frameon=False)
    plt.setp(ax_aux.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax_aux.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title

    for ax in g.axes.flat:
        # ax.get_yaxis().set_tick_params(labelsize='x-large', which="major")
        # labels ao redor dos graficos
        # ax.set_xlabel(ax.get_xlabel(), fontsize='20')
        ax.set_xlabel("", fontsize=SIZE_AXES_LABELS, rotation=30)
        ax.set_ylabel(ax.get_ylabel(), fontsize=SIZE_AXES_LABELS)

        # títulos em cima dos graficos
        if ax.get_title():
            ax.set_title(ax.get_title().split('=')[1],
                         fontsize=SIZE_AXES_TITLE)
        # Valores em cima das barras
        for p in ax.patches:
            height = p.get_height()
            ax.text(p.get_x() + p.get_width() / 2., height + 0.1, int(height), ha="center", fontsize=SIZE_BAR_VALUES)

        ax.yaxis.grid(True, linewidth=1, which="major")
        ax.get_yaxis().set_tick_params(labelsize=SIZE_AXES_TITLE, which="major")
        ax.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_TITLE, rotation=20)

    ax_aux = g.axes[0, 0]
    ax_aux.set_xlabel('', fontsize=SIZE_AXES_LABELS)
    ax_aux.set_ylabel('Number of Projects', fontsize=SIZE_AXES_LABELS)
    # Show graphic
    plt.tight_layout()
    plt.savefig(output_figures_path / 'G1-Q1_numberOfExceptionsTests_bar.pdf', bbox_inches="tight")
    plt.savefig(output_figures_path / 'G1-Q1_numberOfExceptionsTests_bar.png', bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()



############################## RQ1 - PART 2 ##############################
def calculate_NEBTM_NTM_resume(df):
    df_mobile = df[df['platform'] == 'Mobile']
    print(df_mobile.describe())
    df_ds = df[df['platform'] == 'Desktop/Server']
    print(df_ds.describe())
    df_multi = df[df['platform'] == 'Multi-platform']
    print(df_multi.describe())


def violinsPlot_NEBTM_NTM():
    df_filtered = df_RQ[(df_RQ['NEBTM'] >= 0)]
    platformList = df_RQ["platform"].unique()
    domainList = df_RQ["domain"].unique()

    ratio = 0.2

    df_aux = []
    df_belowTo20 = df_filtered[(df_filtered['NEBTM/NTM'] <= ratio)]
    df_belowTo10 = df_filtered[(df_filtered['NEBTM/NTM'] <= 0.1)]
    df_belowTo5 = df_filtered[(df_filtered['NEBTM/NTM'] <= 0.05)]
    print("NEBTM_NTM <= 20% | Total:" + str(len(df_belowTo20)) + " out of " + str(
        len(df_filtered)) + str("({:.2%})").format(len(df_belowTo20) / len(df_filtered)))
    print("NEBTM_NTM <= 10% | Total:" + str(len(df_belowTo10)) + " out of " + str(
        len(df_filtered)) + str("({:.2%})").format(len(df_belowTo10) / len(df_filtered)))
    print("NEBTM_NTM <= 5% | Total:" + str(len(df_belowTo5)) + " out of " + str(
        len(df_filtered)) + str("({:.2%})").format(len(df_belowTo5) / len(df_filtered)))
    for platform in platformList:
        df_platform_total = df_filtered[(df_filtered['platform'] == platform)]
        df_platform_belowTo20 = df_platform_total[(df_platform_total['NEBTM/NTM'] <= ratio)]
        df_platform_belowTo10 = df_platform_total[(df_platform_total['NEBTM/NTM'] <= 0.1)]
        df_platform_belowTo5 = df_platform_total[(df_platform_total['NEBTM/NTM'] <= 0.05)]
        print("NEBTM_NTM <= 20% | " + platform + ":" + str(len(df_platform_belowTo20)) + " out of " + str(len(df_platform_total)) + str("({:.2%})").format(len(df_platform_belowTo20)/len(df_platform_total)))
        print("NEBTM_NTM <= 10% | " + platform + ":" + str(len(df_platform_belowTo10)) + " out of " + str(len(df_platform_total)) + str("({:.2%})").format(len(df_platform_belowTo10)/len(df_platform_total)))
        print("NEBTM_NTM <= 5% | " + platform + ":" + str(len(df_platform_belowTo5)) + " out of " + str(len(df_platform_total)) + str("({:.2%})").format(len(df_platform_belowTo5)/len(df_platform_total)))
        for domain in domainList:
            df_domain_total = df_platform_total[(df_platform_total["domain"] == domain)]
            df_belowTo20 = df_domain_total[(df_domain_total['NEBTM/NTM'] <= ratio)]
            print("NEBTM_NTM <= 20% | " + platform + "/" + domain + ":" + str(len(df_belowTo20)) + " out of " + str(len(df_domain_total)) + str("({:.2%})").format(len(df_belowTo20)/len(df_domain_total)))


    fig = plt.subplots(figsize=(6, 2))
    gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1.5], wspace=.02, hspace=.05)
    ax0 = plt.subplot(gs[0])
    ax1 = plt.subplot(gs[1])
    Util.evaluateOutliersIQR(df_RQ, 'NEBTM/NTM')
    sns.violinplot(x="domain", y="NEBTM/NTM", data=df_RQ, cut=0, inner="box", hue="platform",
                   hue_order=FIGURE_ORDER, scale="width",
                   palette=COLOR_PALETTE, linewidth=1.5, ax=ax0, saturation=1)
    sns.violinplot(x="platform", y="NEBTM/NTM", data=df_RQ, cut=0, inner="box", scale="width",
                   order=FIGURE_ORDER,
                   palette=COLOR_PALETTE, linewidth=1.5, ax=ax1, saturation=1)
    # sns.boxplot(x="domain", y="NEBTM/NTM", data=df_RQ, hue="platform",
    #                hue_order=FIGURE_ORDER,
    #                palette=COLOR_PALETTE, linewidth=1.5, ax=ax0, saturation=1)
    # sns.boxplot(x="platform", y="NEBTM/NTM", data=df_RQ,
    #                order=FIGURE_ORDER,
    #                palette=COLOR_PALETTE, linewidth=1.5, ax=ax1, saturation=1)
    ax0.set(xlabel='', ylabel='', title='', yticks=[0, 0.2, 0.4, 0.6, 0.8, 1])
    ax0.set_ylabel(ax0.get_ylabel(), fontsize=SIZE_AXES_LABELS)
    #ax0.set_ylim(0, 0.4)
    #ax0.legend(title="", loc="upper center")
    ax0.legend(title="", loc="upper center", bbox_to_anchor=(0.75, 1.20), ncol=3, frameon=False)
    ax0.yaxis.grid(True, linewidth=1, which="major")
    plt.setp(ax0.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax0.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title
    ax0.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax0.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax0.xaxis.set_ticks_position('bottom')
    ax0.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, decimals=0))

    ax1.set(xlabel='', ylabel='', title='', xticks=[1], yticks=[0, 0.2, 0.4, 0.6, 0.8, 1])
    # ax1.set_xticks(5)
    ax1.set_xticklabels(['All'])
    ax1.yaxis.grid(True, linewidth=1, which="major")
    ax1.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax1.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, decimals=0))
    ax1.set_yticklabels([])
    #ax1.set_ylim(0, 0.4)
    # Show graphic
    plt.tight_layout()
    plt.savefig(output_figures_path / 'G1-Q2_ratioNumberOfDistinctExceptionTestsViolinPlot_domain.pdf',
                bbox_inches="tight")
    plt.savefig(output_figures_path / 'G1-Q2_ratioNumberOfDistinctExceptionTestsViolinPlot_domain.png',
                bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()

# def violinsPlot_NEBTM_NTM():
#     fig = plt.subplots(figsize=(6, 5))
#     gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1.5], wspace=.02, hspace=.05)
#     ax0 = plt.subplot(gs[0])
#     ax1 = plt.subplot(gs[1])
#     evaluateOutliersIQR(df_RQ, 'NEBTM/NTM')
#     sns.violinplot(x="domain", y="NEBTM/NTM", data=df_RQ, cut=0, inner="box", hue="platform",
#                    hue_order=FIGURE_ORDER, scale="count",
#                    palette=COLOR_PALETTE, linewidth=1.5, ax=ax0)
#     sns.violinplot(x="platform", y="NEBTM/NTM", data=df_RQ, cut=0, inner="box", scale="count",
#                    order=FIGURE_ORDER,
#                    palette=COLOR_PALETTE, linewidth=1.5, ax=ax1)
#     ax0.set(xlabel='', ylabel='', title='', yticks=[0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
#     ax0.set_ylabel(ax0.get_ylabel(), fontsize=SIZE_AXES_LABELS)
#     #ax0.set_ylim(0, 0.4)
#     ax0.legend(title="", loc="upper right")
#     ax0.yaxis.grid(True, linewidth=1, which="major")
#     plt.setp(ax0.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
#     plt.setp(ax0.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title
#     ax0.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
#     ax0.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
#     ax0.xaxis.set_ticks_position('bottom')
#     ax0.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, decimals=0))
#
#     ax1.set(xlabel='', ylabel='', title='', xticks=[1], yticks=[0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
#     # ax1.set_xticks(5)
#     ax1.set_xticklabels(['all'])
#     ax1.yaxis.grid(True, linewidth=1, which="major")
#     ax1.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
#     ax1.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
#     ax1.xaxis.set_ticks_position('bottom')
#     ax1.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, decimals=0))
#     ax1.set_yticklabels([])
#     #ax1.set_ylim(0, 0.4)
#     # Show graphic
#     plt.tight_layout()
#     plt.savefig(output_figures_path / 'G1-Q2_ratioNumberOfDistinctExceptionTestsViolinPlot_domain.pdf',
#                 bbox_inches="tight")
#     plt.savefig(output_figures_path / 'G1-Q2_ratioNumberOfDistinctExceptionTestsViolinPlot_domain.png',
#                 bbox_inches="tight")
#     if (SHOW_FIGURE):
#         plt.show()
#     plt.clf()
############################## RQ1 - PART 3 ##############################


def violinsPlot_NDTE_NDUE():

    df_ndte = df_RQ[(df_RQ['NDTE'] >= 0)]
    df_diff = df_RQ[(df_RQ['NDTE'] - df_RQ['NDTUE'] > 0)]
    ratio = len(df_diff) / len(df_ndte)
    print("Number of Projects where NDTE > NDTUE " + str(len(df_diff)) + " de " + str(len(df_ndte)) + " " + str(ratio))
    fig = plt.subplots(figsize=(6, 2))
    gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1.5], wspace=.02, hspace=.05)
    ax0 = plt.subplot(gs[0])
    ax1 = plt.subplot(gs[1])
    Util.evaluateOutliersIQR(df_RQ, 'NDTE/NDUE')
    sns.violinplot(x="domain", y="NDTE/NDUE", data=df_RQ, cut=0, inner="box", hue="platform",
                   hue_order=FIGURE_ORDER, scale="width",
                   palette=COLOR_PALETTE, linewidth=1.5, ax=ax0, saturation=1)
    sns.violinplot(x="platform", y="NDTE/NDUE", data=df_RQ, cut=0, inner="box", scale="width",
                   order=FIGURE_ORDER,
                   palette=COLOR_PALETTE, linewidth=1.5, ax=ax1, saturation=1)
    # sns.boxplot(x="domain", y="NDTE/NDUE", data=df_RQ, hue="platform",
    #                hue_order=FIGURE_ORDER,
    #                palette=COLOR_PALETTE, linewidth=1.5, ax=ax0, saturation=1)
    # sns.boxplot(x="platform", y="NDTE/NDUE", data=df_RQ,
    #                order=FIGURE_ORDER,
    #                palette=COLOR_PALETTE, linewidth=1.5, ax=ax1, saturation=1)
    ax0.set(xlabel='', ylabel='', title='', yticks=[0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4])
    ax0.set_ylabel(ax0.get_ylabel(), fontsize=SIZE_AXES_LABELS)
    # ax0.set_ylim(0, 0.4)
    # ax0.legend(title="", loc="upper center")
    ax0.legend(title="", loc="upper center", bbox_to_anchor=(0.75, 1.20), ncol=3, frameon=False)
    ax0.yaxis.grid(True, linewidth=1, which="major")
    plt.setp(ax0.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax0.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title
    ax0.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax0.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax0.xaxis.set_ticks_position('bottom')
    ax0.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, decimals=0))

    ax1.set(xlabel='', ylabel='', title='', xticks=[1], yticks=[0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4])
    # ax1.set_xticks(5)
    ax1.set_xticklabels(['All'])
    ax1.yaxis.grid(True, linewidth=1, which="major")
    ax1.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax1.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, decimals=0))
    ax1.set_yticklabels([])
    # ax1.set_ylim(0, 0.4)
    # Show graphic
    plt.tight_layout()
    plt.savefig(output_figures_path / 'G1-Q3_ratioNumberOfTestedExceptionsViolinPlot_domain.pdf',
                bbox_inches="tight")
    plt.savefig(output_figures_path / 'G1-Q3_ratioNumberOfTestedExceptionsViolinPlot_domain.png',
                bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()




def violinsPlot_NDTUE_NDUE():
    df_filtered = df_RQ[(df_RQ['NEBTM'] >= 0)]
    platformList = df_RQ["platform"].unique()
    domainList = df_RQ["domain"].unique()
    ratio = 0.10
    df_aux = []
    df_below10 = df_filtered[(df_filtered['NDTUE/NDUE'] <= ratio)]
    print("NDTUE/NDUE <= 10% | Total:" + str(len(df_below10)) + " out of " + str(
        len(df_filtered)) + str("({:.2%})").format(len(df_below10) / len(df_filtered)))
    for domain in domainList:
        df_domain_total = df_filtered[(df_filtered["domain"] == domain)]
        df_domain_below10 = df_domain_total[(df_domain_total['NDTUE/NDUE'] <= ratio)]
        print("NDTUE/NDUE <= 10% | " + domain + ":" + str(len(df_domain_below10)) + " out of " + str(
            len(df_domain_total)) + str("({:.2%})").format(len(df_domain_below10) / len(df_domain_total)))
    for platform in platformList:
        df_platform_total = df_RQ[(df_RQ['platform'] == platform)]
        df_platform_below10 = df_platform_total[(df_platform_total['NDTUE/NDUE'] <= ratio)]
        print("NDTUE/NDUE <= 10% | " + platform + ":" + str(len(df_platform_below10)) + " out of " + str(len(df_platform_total)) + str("({:.2%})").format(len(df_platform_below10)/len(df_platform_total)))
        for domain in domainList:
            df_domain_total = df_platform_total[(df_platform_total["domain"] == domain)]
            df_domain_below10 = df_domain_total[(df_domain_total['NDTUE/NDUE'] <= ratio)]
            print("NDTUE/NDUE <= 10% | " + platform + "/" + domain + ":" + str(len(df_domain_below10)) + " out of " + str(len(df_domain_total)) + str("({:.2%})").format(len(df_domain_below10)/len(df_domain_total)))

    fig = plt.subplots(figsize=(6, 2))
    gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1.5], wspace=.02, hspace=.05)
    ax0 = plt.subplot(gs[0])
    ax1 = plt.subplot(gs[1])
    Util.evaluateOutliersIQR(df_RQ, 'NDTUE/NDUE')
    sns.violinplot(x="domain", y="NDTUE/NDUE", data=df_RQ, cut=0, inner="box", hue="platform",
                   hue_order=FIGURE_ORDER, scale="width",
                   palette=COLOR_PALETTE, linewidth=1.5, ax=ax0, saturation=1)
    sns.violinplot(x="platform", y="NDTUE/NDUE", data=df_RQ, cut=0, inner="box", scale="width",
                   order=FIGURE_ORDER,
                   palette=COLOR_PALETTE, linewidth=1.5, ax=ax1, saturation=1)
    # sns.boxplot(x="domain", y="NDTUE/NDUE", data=df_RQ, hue="platform",
    #                hue_order=FIGURE_ORDER,
    #                palette=COLOR_PALETTE, linewidth=1.5, ax=ax0, saturation=1)
    # sns.boxplot(x="platform", y="NDTUE/NDUE", data=df_RQ,
    #                order=FIGURE_ORDER,
    #                palette=COLOR_PALETTE, linewidth=1.5, ax=ax1, saturation=1)
    ax0.set(xlabel='', ylabel='', title='', yticks=[0, 0.2, 0.4, 0.6, 0.8, 1])
    ax0.set_ylabel(ax0.get_ylabel(), fontsize=SIZE_AXES_LABELS)
    # ax0.set_ylim(0, 0.4)
    # ax0.legend(title="", loc="upper center")
    ax0.legend(title="", loc="upper center", bbox_to_anchor=(0.75, 1.20), ncol=3, frameon=False)
    ax0.yaxis.grid(True, linewidth=1, which="major")
    plt.setp(ax0.get_legend().get_texts(), fontsize=SIZE_LEGEND_TEXT)  # for legend text
    plt.setp(ax0.get_legend().get_title(), fontsize=SIZE_LEGEND_TITLE)  # for legend title
    ax0.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax0.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax0.xaxis.set_ticks_position('bottom')
    ax0.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, decimals=0))

    ax1.set(xlabel='', ylabel='', title='', xticks=[1], yticks=[0, 0.2, 0.4, 0.6, 0.8, 1])
    # ax1.set_xticks(5)
    ax1.set_xticklabels(['All'])
    ax1.yaxis.grid(True, linewidth=1, which="major")
    ax1.get_yaxis().set_tick_params(labelsize=SIZE_AXES_VALUES, which="major")
    ax1.get_xaxis().set_tick_params(direction='out', labelsize=SIZE_AXES_VALUES)
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, decimals=0))
    ax1.set_yticklabels([])
    # ax1.set_ylim(0, 0.4)
    # Show graphic
    plt.tight_layout()
    plt.savefig(output_figures_path / 'G1-Q3_ratioNumberOfTestedAndUsedExceptionsViolinPlot_domain.pdf',
                bbox_inches="tight")
    plt.savefig(output_figures_path / 'G1-Q3_ratioNumberOfTestedAndUsedExceptionsViolinPlot_domain.png',
                bbox_inches="tight")
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def heatMap():
    fig, ax = plt.subplots(figsize=(10, 10))
    sns.heatmap(df_RQ.corr(), annot=True, fmt=".2f")
    # Show graphic
    # plt.tight_layout()
    # plt.savefig(output_figures_path / 'Fig3_ratioNDTUE_NDUE_BoxPlot.pdf', bbox_inches="tight")
    plt.savefig(output_figures_path / 'VisaoGeral_Correlacao.png')
    if (SHOW_FIGURE):
        plt.show()
    plt.clf()


def autolabelVertical(rects, ax, xpos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    xpos = xpos.lower()  # normalize the case of the parameter
    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0.5, 'right': 0.57, 'left': 0.43}  # x_txt = x + w*off

    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() * offset[xpos], 1.01 * height,
                '{}'.format(height), ha=ha[xpos], va='bottom')


def autolabelHorizontal(rects, ay, ypos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    ypos = ypos.lower()  # normalize the case of the parameter
    va = {'center': 'center', 'top': 'bottom', 'bottom': 'top'}
    offset = {'center': 0.5, 'top': 0.57, 'bottom': 0.43}  # y_txt = y + w*off

    for rect in rects:
        width = rect.get_width()
        ay.text(1.01 * width, rect.get_y() + rect.get_height() * offset[ypos],
                '{}'.format(width), va=va[ypos], ha='left')





# Generating Table III
generateDataSetRQ1()



#
# RQ1-PART1
barPlotNumberOfExceptionalTests_barplot()
#numberOfTestMethodsPerProject_histogram()
#numberOfExceptionalBehaviorTestMethodsPerProject_histogram()
#numberOfExceptionalBehaviorTestMethodsPerProject_violinPlot()
# numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfTestMethods_scatterplot()
#numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfTestMethods_linearregression()
# numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfExceptionsUses_linearregression()
# numberOfExceptionalBehaviorTestMethodsPerProjectXNumberOfContributors_linearregression()

calculateCorrelations()

# RQ1-PART2
violinsPlot_NEBTM_NTM()
#
# RQ1-PART3
violinsPlot_NDTUE_NDUE()
#
# RQ1-PART4
violinsPlot_NDTE_NDUE()


#heatMap()
