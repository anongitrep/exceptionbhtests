package projectsfinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.client.*;
import exceptionhunter.ProjectsManagerSingleton;
import org.bson.Document;
import org.eclipse.egit.github.core.Commit;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.RepositoryTag;
import org.eclipse.egit.github.core.SearchRepository;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.CommitService;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ProjectsFinder {

    private static final boolean ONLY_LATEST_TAG = false;
    private static Logger logger = LoggerFactory.getLogger("ExceptionHunter");
    private static int NUMBER_OF_REPOSITORIES_PER_DOMAIN = 100;
    private static Map<String, Map<String, Integer>> REPOSITORIES_COUNTER;
    private static GitHubClient client;
    private static CommitService commitService;
    private static RepositoryService service;
    private static MongoClient mongoClient =  MongoClients.create("mongodb://localhost:27017");
    private static MongoDatabase mongoDatabase = mongoClient.getDatabase("ExceptionHunter_ProjectsFinder");
    private static MongoCollection<Document> mongoCollection;

    public static void main(String[] args) {
        logger.info("Starting to extract information about selected projects");
        mongoCollection = mongoDatabase.getCollection("ExceptionHunterProjectsDB_100_per_domain");
        client = new GitHubClient().setOAuth2Token("542ee9aa6ba4d490d3dc693381eb22960dcf1d69");
        commitService = new CommitService(client);
        service = new RepositoryService(client);
        REPOSITORIES_COUNTER = new HashMap<>();
        List<String> domainList = Arrays.asList("framework", "library", "tool");
        List<String> platformList = Arrays.asList("mobile", "desktop/servers");
        for (String platform : platformList){
            Map<String, Integer> domainMap = new HashMap<>();
            REPOSITORIES_COUNTER.put(platform, domainMap);
            for(String domain : domainList){
                domainMap.put(domain, 0);
                String query = "language:java sort:stars";

                if (domain == "framework"){
                    query += " framework NOT library NOT tool";
                } else if (domain == "library"){
                    query += " library NOT framework NOT tool";
                } else if (domain == "tool"){
                    query += " tool NOT framework NOT library";
                }

                if (platform == "mobile"){
                    query += " android";
                } else {
                    query += " NOT android NOT mobile";
                }
                extractDataFromList(query, platform, domain);
            }
        }

    }

    public ProjectsFinder() {
        super();
    }

    private static void extractDataFromList(String query, String platform, String domain) {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Type gsonType = new TypeToken<HashMap<String, String>>(){}.getType();

        logger.info("Client has " + client.getRequestLimit() + " as request limit and " + client.getRemainingRequests() + " remaining requests");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        for(int pageNumber = 0; REPOSITORIES_COUNTER.get(platform).get(domain) < NUMBER_OF_REPOSITORIES_PER_DOMAIN; pageNumber++) {

            List<SearchRepository> repositoryList = null;
            try {
                logger.info("Searching for repositories with the query: " + query);
                repositoryList = service.searchRepositories(query, pageNumber);
                logger.info("Found " + repositoryList.size() + " repositories in page " + pageNumber);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                for (SearchRepository repoAux : repositoryList) {

                    String owner = repoAux.getOwner();
                    String projectName = repoAux.getName();

                    BasicDBObject whereQuery = new BasicDBObject();
                    whereQuery.put("projectName", projectName);
                    Document myDoc = mongoCollection.find(whereQuery).first();
                    if( myDoc != null){
                        logger.info("Project "+ projectName + " already in the database! Skipping.");
                        continue;
                    }


                    Repository repo = null;

                    try {
                        logger.info("Extracting data from " + owner + "/" + projectName);
                        repo = service.getRepository(owner, projectName);
                    } catch (IOException e) {
                        logger.error("Failure! Project " + owner + "/" + projectName + " not found! Skipping.");
                        logger.error(e.toString());
                        continue;
                    }
                    Map project = new HashMap<String, String>();
                    String description = repo.getDescription();

                    description = description == null ? "" : description; //sometimes the description field is empty and return a null pointer
                    checkRemainingRequests();
                    project.put("projectOwner", owner);
                    project.put("projectName", projectName);
                    project.put("description", description);
                    project.put("contributors", service.getContributors(repo, false).size());
                    project.put("stars", repo.getWatchers());
                    project.put("gitRepo", repo.getHtmlUrl());
                    checkRemainingRequests();
//                    String projectAndDescription = projectName.toLowerCase() + " " + description.toLowerCase();
//                    List<String> platformKeywords = new ArrayList<>();
//                    List<String> domainKeywords = new ArrayList<>();
//                    String platform = "desktop/servers";
//                    if (projectAndDescription.contains("android")) {
//                        platformKeywords.add("android");
//                        platform = "mobile";
//                    }
//                    if (projectAndDescription.contains("mobile")) {
//                        platformKeywords.add("mobile");
//                        platform = "mobile";
//                    }
                    project.put("platform", platform);
//                    String domain = "others";
//                    if (projectAndDescription.contains("library")){
//                        domainKeywords.add("library");
//                        domain = "library";
//                    }
//                    if (projectAndDescription.contains("libraries")){
//                        domainKeywords.add("library");
//                        domain = "library";
//                    }
//                    if (projectAndDescription.contains("tool")){
//                        domainKeywords.add("tool");
//                        //only change the domain if it is not identified as library
//                        if (domain == "others"){
//                            domain = "tool";
//                        }
//                    }
//                    project.put("platformKeywords", platformKeywords.toString().replaceAll("(\\[|\\])", ""));
//                    project.put("domainKeywords", domainKeywords.toString().replaceAll("(\\[|\\])", ""));
                    project.put("domain", domain);
                    project.put("lastUpdatedAt", dateFormat.format(repo.getUpdatedAt()));
                    project.put("size", repo.getSize());
                    project.put("createdAt", dateFormat.format(repo.getCreatedAt()));
                    project.put("sourceRoots", new String[]{"all"});
                    project.put("active", true);
                    checkRemainingRequests();

                    Map tags = new HashMap<Date, String>();
                    Date latestDate = new GregorianCalendar(1970, 0, 1).getTime();
                    String latestTagName = "";
                    List<RepositoryTag> tagsList = service.getTags(repo);
                    logger.info("Found " + tagsList.size() + " tags");
                    if (tagsList.size() > 0) {
                        logger.info("Extracting data from tags. It may take a while.");
                        for (RepositoryTag tag : tagsList) {
                            Commit commit = commitService.getCommit(repo, tag.getCommit().getSha()).getCommit();
                            Date commitDate = commit.getCommitter().getDate();
                            String tagName = tag.getName().replace(".","#");
                            if (ONLY_LATEST_TAG) {
                                if (commitDate.compareTo(latestDate) > 0) {
                                    latestDate = commitDate;
                                    latestTagName = tagName;
                                }
                            } else {
                                tags.put(tagName, dateFormat.format(commitDate));
                            }
                            checkRemainingRequests();
                        }
                        if (ONLY_LATEST_TAG) {
                            tags.put(latestTagName, dateFormat.format(latestDate));
                        }
                    } else {
                        //if none tag was found, skip to the next project
                        continue;
                        //tags.put("master", dateFormat.format(repo.getUpdatedAt()));
                    }

                    checkRemainingRequests();
                    project.put("tags", tags);

                    project.put("contributors", service.getContributors(repo, false).size());

                    project.put("includedAt", dateFormat.format(new Date()));
                    logger.info("Success! Data extracted.");
                    String json = gson.toJson(project, gsonType);
                    Document doc = Document.parse(json);
                    mongoCollection.insertOne(doc);
                    int newTotal = REPOSITORIES_COUNTER.get(platform).get(domain) + 1;
                    REPOSITORIES_COUNTER.get(platform).put(domain, newTotal);
                    logger.info("Project saved to mongoDB");
                    logger.info("*** Client has " + client.getRemainingRequests() + " remaining requests ***");

                    checkRemainingRequests();

                    if(REPOSITORIES_COUNTER.get(platform).get(domain) >= NUMBER_OF_REPOSITORIES_PER_DOMAIN){
                        break;
                    }
                }
            } catch (IOException e) {
                logger.error(e.toString());
                logger.error("Failure! Data not extracted! Skipping.");
            }
        }
    }


    private static void checkRemainingRequests() {
        int remainingRequests = client.getRemainingRequests();
        while (remainingRequests > -1 && remainingRequests < 100){
            try{
                logger.info("*** Pausing the execution for 5 minutes. Waiting GitHub authorization for more requests.*** Remaining:  " + remainingRequests);
                Thread.sleep(300000);
                client = new GitHubClient().setOAuth2Token("542ee9aa6ba4d490d3dc693381eb22960dcf1d69");
                commitService = new CommitService(client);
                service = new RepositoryService(client);
                remainingRequests = client.getRemainingRequests();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
