package projectsfinder;


import com.mongodb.BasicDBObject;
import com.mongodb.client.*;
import org.bson.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import static com.mongodb.client.model.Indexes.descending;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;

public class ProjectsPostExtraction {

    private static final int BASE_SIZE = 1000;

    public static void main(String[] args) throws IOException {

        MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase mongoDatabase = mongoClient.getDatabase("ExceptionHunter_ProjectsFinder");

        //sortResizeAndExtract(mongoDatabase);
        extract(mongoDatabase);


    }

    private static void extract(MongoDatabase mongoDatabase) throws IOException {
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection("ExceptionHunterProjectsDB_100_per_domain");
        FindIterable<Document> cursor2 = mongoCollection.find().projection(fields(excludeId()));
        extractFromMongoToJson(cursor2);
    }

    private static void sortResizeAndExtract(MongoDatabase mongoDatabase) throws IOException {
        //sort and select the first 1000
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection("ExceptionHunterProjectsDB_100_per_domain");
        FindIterable<Document> cursor = mongoCollection.find().sort(descending("stars")).limit(BASE_SIZE);
        MongoCollection<Document> collectionTopX = mongoDatabase.getCollection("ExceptionHunterProjectsDB_Top" + BASE_SIZE);

        for (Document doc : cursor) {
            collectionTopX.insertOne(doc);
        }

        MongoCollection<Document> collectionWithTag = mongoDatabase.getCollection("ExceptionHunterProjectsDB_500_TAGGED");
        BasicDBObject query = new BasicDBObject("tags.master", new BasicDBObject("$exists", false));

        FindIterable<Document> cursor2 = collectionTopX.find(query).projection(fields(excludeId()));
        extractFromMongoToJson(cursor2);

    }

    private static void extractFromMongoToJson(FindIterable<Document> cursor2) throws IOException {
        Path fullPath = Paths.get("src", "main", "resources", "auxFolder", "result_" + new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date()) + ".json");
        FileWriter fw = new FileWriter(fullPath.toFile());
        BufferedWriter writer = new BufferedWriter(fw);;
        int i = 0;
        for (Document doc2 : cursor2) {
            if(i++ > 0){
                writer.write("," + System.lineSeparator());
            }
            writer.write(doc2.toJson());
            writer.flush();
            //collectionWithTag.insertOne(doc2);
        }
    }
}
