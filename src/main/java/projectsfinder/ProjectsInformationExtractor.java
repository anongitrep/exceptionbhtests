package projectsfinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import exceptionhunter.ProjectsManagerSingleton;
import org.eclipse.egit.github.core.Commit;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.RepositoryCommit;
import org.eclipse.egit.github.core.RepositoryTag;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.CommitService;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ProjectsInformationExtractor {

    private static final boolean ONLY_LATEST_TAG = true;
    private static Logger logger = LoggerFactory.getLogger("ExceptionHunter");

    public static void main(String[] args) {
        extractDataFromList();

    }

    public ProjectsInformationExtractor() {
        super();
    }

    private static void extractDataFromList() {
        logger.info("Starting to extract information about select projects");
        String csvPath = ProjectsManagerSingleton.class.getClassLoader().getResource("ExtractDataFromList.txt").getPath();
        BufferedReader reader = null;
        String line;
        GitHubClient client = new GitHubClient().setOAuth2Token("542ee9aa6ba4d490d3dc693381eb22960dcf1d69");
        logger.info("Client has " + client.getRequestLimit() + " as request limit and " + client.getRemainingRequests() + " remaining requests");
        RepositoryService service = new RepositoryService(client);
        CommitService commitService = new CommitService(client);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");


        List projects = new LinkedList<HashMap<String, String>>();

        try {
            reader = new BufferedReader(new FileReader(csvPath));
            while ((line = reader.readLine()) != null) {
                //the line that starts with # is just a commentary
                if (line.startsWith("#") || line.isEmpty()) {
                    continue;
                }

                String[] data = line.split(",");
                Repository repo = null;

                try {
                    logger.info("Extracting data from " + data[0] + "/" + data[1]);
                    repo = service.getRepository(data[0], data[1]);
                } catch (IOException e) {
                    logger.error("Failure! Project " + data[0] + "/" + data[1] + " not found! Skipping.");
                    logger.error(e.toString());
                    continue;
                }
                Map project = new HashMap<String, String>();
                project.put("projectOwner", data[0]);
                project.put("projectName", data[1]);
                project.put("domain", data[2]);
                project.put("stars", repo.getWatchers());
                project.put("gitRepo", repo.getHtmlUrl());
                project.put("lastUpdatedAt", dateFormat.format(repo.getUpdatedAt()));
                project.put("size", repo.getSize());
                project.put("createdAt", dateFormat.format(repo.getCreatedAt()));
                project.put("description", repo.getDescription());
                project.put("sourceRoots", new String[]{"all"});


                Map tags = new HashMap<Date, String>();
                Date latestDate = new GregorianCalendar(1970, 0, 1).getTime();
                String latestTagName = "";
                List<RepositoryTag> tagsList = service.getTags(repo);
                if(tagsList.size() > 0){
                    for (RepositoryTag tag : service.getTags(repo)) {
                        Commit commit = commitService.getCommit(repo, tag.getCommit().getSha()).getCommit();
                        Date commitDate = commit.getCommitter().getDate();
                        if (ONLY_LATEST_TAG){
                            if (commitDate.compareTo(latestDate) > 0) {
                                latestDate = commitDate;
                                latestTagName = tag.getName();
                            }
                        } else {
                            tags.put(tag.getName(), dateFormat.format(commitDate));
                        }
                    }
                    if (ONLY_LATEST_TAG){
                        tags.put(latestTagName, dateFormat.format(latestDate));
                    }
                } else {
                    //if none tag was found, use the master branch
                    tags.put("master", dateFormat.format(repo.getUpdatedAt()));
                }


                project.put("tags", tags);

                project.put("contributors", service.getContributors(repo, false).size());

                project.put("includedAt", dateFormat.format(new Date()));
                project.put("active", true);
                projects.add(project);
                logger.info("Success! Data extracted.");
                logger.info("*** Client has " + client.getRemainingRequests() + " remaining requests ***");

                while (client.getRemainingRequests() < 1000){
                    try{
                        logger.info("*** Pausing the execution for 5 minutes. Waiting more GitHub requests authorization.***");
                        Thread.sleep(300000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
            reader.close();
        } catch (FileNotFoundException e) {
            logger.error(e.toString());
            logger.error("Failure! Data not extracted! Skipping.");
        } catch (IOException e) {
            logger.error(e.toString());
            logger.error("Failure! Data not extracted! Skipping.");
        }

        writeToFile(projects);
    }

    private static void writeToFile(List projects) {
        //String fullPath = "C:\\Users\\Fdalton\\IdeaProjects\\exceptionhunter\\src\\main\\resources\\result.json";
        Path fullPath = Paths.get("src", "main", "resources", "auxFolder", "result_" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss") + ".json");
        logger.info("Writing results to " + fullPath.toString());
        try (Writer writer = new FileWriter(fullPath.toString())) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Type gsonType = new TypeToken<List<HashMap<String, String>>>() {
            }.getType();
            gson.toJson(projects, gsonType, writer);
            logger.info("File saved to " + fullPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
