package exceptionhunter.extractors;

import com.google.gson.annotations.Expose;
import exceptionhunter.models.*;
import exceptionhunter.models.assertj.AssertJ_assertThatExceptionNameModel;
import exceptionhunter.models.assertj.AssertJ_assertThatExceptionOfTypeModel;
import exceptionhunter.models.assertj.AssertJ_assertThatModel;
import exceptionhunter.models.assertj.AssertJ_assertThatThrownByModel;
import exceptionhunter.models.commom.Commom_FailCallModel;
import exceptionhunter.models.junit.JUnit_ExpectExceptionTestCallModel;
import exceptionhunter.models.junit.JUnit_assertThrowsModel;
import exceptionhunter.models.junit.JUnit_ExpectedExceptionTestAnnotationModel;
import exceptionhunter.models.testng.TestNG_ExpectedExceptionsTestAnnotationModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StatisticsExtractor {


    private ModelExtractor models;

    private CombinedTypeExtractor combinedTypeExtractor;

    private boolean isFrameworksCounted = false; //flag to tells when the tests are already counted by framework

    //No primeiro loop é preciso percorrer o projeto para coletar as CustomExceptions, pois elas sao necessárias para computador boa tarde das metricas
    @Expose
    private List<String> customCreatedExceptions; //populado pelo metodo calculateCustomExceptionsCreatedCounter

    @Expose
    private Map<String, Integer> throwsStandardOrThirdPartyExceptionsCounter; //populado pelo metodo calculateTotalOfThrowsStandardOrThirdPartyExceptions(

    @Expose
    private Map<String, Integer> throwsCustomExceptionsCounter; //populado pelo metodo calculateTotalOfThrowsCustomExceptions

    @Expose
    private Map<String, Integer> throwsNotIdentifiedExceptionsCounter; //populado pelo metodo calculateTotalOfThrowsCustomExceptions

    @Expose
    private Map<String, Integer> throwStandardOrThirdPartyExceptionsExceptionsCounter; //populado pelo metodo calculateTotalOfThrowStatementsStandardOrThirdPartyExceptions

    @Expose
    private Map<String, Integer> throwCustomExceptionsCounter; //populado pelo metodo calculateTotalOfThrowStatementsCustomExceptions

    @Expose
    private Map<String, Integer> throwNotIdentifiedExceptionsCounter; //populado pelo metodo calculateTotalOfThrowStatementsCustomExceptions

    @Expose
    private Map<String, Integer> catchStandardOrThirdPartyExceptionsCounter; //populado pelo metodo calculateTotalOfCatchStandardOrThirdPartyExceptionsCounter(

    @Expose
    private Map<String, Integer> catchCustomExceptionsCounter; //populado pelo metodo calculateTotalOfCatchCustomExceptionsCounter

    @Expose
    private Map<String, Integer> catchNotIdentifiedExceptionsCounter; //populado pelo metodo calculateTotalOfCatchCustomExceptionsCounter

    @Expose
    private Map<String, Integer> standardOrThirdPartyExceptionsUsed; //populado pelo metodo calculateTotalOfCatchCustomExceptionsCounter

    @Expose
    private Map<String, Integer> customExceptionsUsed; //populado pelo metodo calculateTotalOfCatchCustomExceptionsCounter

    @Expose
    private Map<String, Integer> notIdentifiedExceptionsUsed; //populado pelo metodo calculateTotalOfCatchCustomExceptionsCounter

    @Expose
    private Map<String, Integer> testedStandardOrThirdPartyExceptionsCounter; //populado pelo metodo calculateTestedExceptionsCounter

    @Expose
    private Map<String, Integer> testedCustomExceptionsCounter; //populado pelo metodo calculateTestedCustomExceptionsCounter

    @Expose
    private Map<String, Integer> testedAndUsedStandardOrThirdPartyExceptionsCounter; //populado pelo metodo calculateTestedExceptionsCounter

    @Expose
    private Map<String, Integer> testedAndUsedCustomExceptionsCounter; //populado pelo metodo calculateTestedCustomExceptionsCounter
    @Expose
    private Map<String, Integer> testedNotIdentifiedExceptionsCounter; //populado pelo metodo calculateTestedCustomExceptionsCounter

    @Expose
    private Map<String, Integer> customExceptionsTestMethodCounter; //populado pelo metodo

    @Expose
    private Map<String, Integer> standardOrThirdPartyExceptionsTestMethodCounter; //populado pelo metodo

    @Expose
    private Map<String, Integer> notIdentifiedExceptionsTestMethodCounter; //populado pelo metodo

    @Expose
    private Map<String, Integer> distinctExceptionsTestMethodCounter; //populado pelo metodo


    /**************** METRICS *********************/
    @Expose
    private Integer totalNumberOfTestsByAnnotation;
    @Expose
    private Integer totalNumberOfTestsByInheritance;
    @Expose
    private Integer totalNumberOfTestsWithJUnitImports;
    @Expose
    private Integer totalNumberOfTestsWithTestNGImports;
    @Expose
    private Integer totalNumberOfTestsWithAssertJImports;
    @Expose
    private Integer totalNumberOfTestsWithNotIdentifiedImports;
    @Expose
    private Integer totalNumberOfTestMethods;
    @Expose
    private Integer totalNumberOfCreatedCustomExceptions;
    @Expose
    private Integer totalNumberOfDistinctUsedCustomExceptions;
    @Expose
    private Integer totalNumberOfDistinctUsedNotIdentifiedExceptions;
    @Expose
    private Integer totalNumberOfDistinctUsedStandardOrThirdPartyExceptions;
    @Expose
    private Integer totalNumberOfDistinctUsedExceptions;
    @Expose
    private Integer totalNumberOfThrowsStatementsStandardOrThirdPartyExceptions;
    @Expose
    private Integer totalNumberOfThrowsStatementCustomExceptions;
    @Expose
    private Integer totalNumberOfThrowsStatementNotIdentifiedExceptions;
    @Expose
    private Integer totalNumberOfThrowsStatements;
    @Expose
    private Integer totalNumberOfThrowStatementsStandardOrThirdPartyExceptions;
    @Expose
    private Integer totalNumberOfThrowStatementCustomExceptions;
    @Expose
    private Integer totalNumberOfThrowStatementNotIdentifiedExceptions;
    @Expose
    private Integer totalNumberOfThrowStatements;
    @Expose
    private Integer totalNumberOfAllTestedNotIdentifiedExceptions; //consider the exceptions within the test suite
    @Expose
    private Integer totalNumberOfAllTestedStandardOrThirdPartyExceptions; //consider the exceptions within the test suite
    @Expose
    private Integer totalNumberOfAllTestedCustomExceptions; //consider the exceptions within the test suite
    @Expose
    private Integer totalNumberOfAllTestedExceptions; //consider the exceptions within the test suite
    @Expose
    private Integer totalNumberOfDistinctTestedStandardOrThirdPartyExceptions;
    @Expose
    private Integer totalNumberOfDistinctTestedCustomExceptions;
    @Expose
    private Integer totalNumberOfDistinctTestedExceptions;
    @Expose
    private Integer totalNumberOfCustomExceptionsTestMethods;
    @Expose
    private Integer totalNumberOfStandardOrThirdPartyExceptionsTestMethods;
    @Expose
    private Integer totalNumberOfNotIdentifiedExceptionsTestMethods;
    @Expose
    private Integer totalNumberOfExceptionTests; //has repeated methods
    @Expose
    private Integer totalNumberOfExceptionalBehaviorTestMethods; //distinct methods

    /************** commom **************/
    @Expose
    private Integer commom_totalNumberOfFailInsideTryScope;
    @Expose
    private Integer commom_totalNumberOfFailInsideCatchScope;
    @Expose
    private Integer commom_totalNumberOfFailOutsideTryCatchScope;
    @Expose
    private Integer commom_totalNumberOfFailCalls;

    /************** JUNIT **************/
    @Expose
    private Integer junit_totalNumberOfExpectedAttribute;
    @Expose
    private Integer junit_totalNumberOfExpectCalls;
    @Expose
    private Integer junit_totalNumberOfAssertThrows;

    /************** ASSERTJ **************/
    @Expose
    private Integer assertj_totalNumberOfAsserts;
    @Expose
    private Integer assertj_totalNumberOfAssertThatExceptionName;
    @Expose
    private Integer assertj_totalNumberOfAssertThatExceptionOfType;
    @Expose
    private Integer assertj_totalNumberOfAssertThat;
    @Expose
    private Integer assertj_totalNumberOfAssertThatThrownBy;

    /************** TESTNG **************/
    @Expose
    private Integer testNG_totalNumberOfExpectedExceptionsAttribute;

    //CATCHS
    @Expose
    private Integer totalNumberOfCatchStatementCustomExceptions;
    @Expose
    private Integer totalNumberOfCatchStatementsStandardOrThirdPartyExceptions;
    @Expose
    private Integer totalNumberOfCatchStatementNotIdentifiedExceptions;
    @Expose
    private Integer totalNumberOfCatchStatements;

    //USES
    @Expose
    private Integer totalNumberOfCustomExceptionsUses;
    @Expose
    private Integer totalNumberOfStandardOrThirdPartyExceptionsUses;
    @Expose
    private Integer totalNumberOfNotIdentifiedExceptionsUses;
    @Expose
    private Integer totalNumberOfExceptionsUses;

    //percentuais
    //@Expose
    private Double totalAbsolutPercentegeOfNotIdentifiedExceptionsTestMethods;
    //@Expose
    private Double totalAbsolutPercentegeOfStandardOrThirdPartyExceptionsTestMethods;
    //@Expose
    private Double totalAbsolutPercentageCustomExceptionsTestMethods;
    //@Expose
    private Double totalAbsolutPercentageExceptionsTestMethods;
    //@Expose
    private Double totalAbsolutPercentegeOfStandardOrThirdPartyExceptionsTested;
    //@Expose
    private Double totalAbsolutPercentegeOfNotIdentifiedExceptionsTested;
    //@Expose
    private Double totalAbsolutPercentageCustomExceptionsTested;
    //@Expose
    private Double totalAbsolutPercentageExceptionsTested;
    //@Expose
    private Double totalRelativePercentegeOfStandardOrThirdPartyExceptionsTested;
    //@Expose
    private Double totalRelativePercentegeOfNotIdentifiedExceptionsTested;
    //@Expose
    private Double totalRelativePercentageCustomExceptionsTested;


    //Se essa flag estiver true, entao todos os pontos (.) das strings serao trocados por underlines (.)
    //Isso é necessário porque o mongoDB não aceita pontos em chaves
    private boolean adjustStringToMongoDB;
    private boolean isTestedExceptionsCalculated;



    public StatisticsExtractor(CombinedTypeExtractor combinedTypeExtractor, ModelExtractor models, boolean adjustStringToMongoDB) {
        this.models = models;
        this.combinedTypeExtractor = combinedTypeExtractor;
        this.isTestedExceptionsCalculated = false;

        this.testedStandardOrThirdPartyExceptionsCounter = new HashMap<String, Integer>();
        this.testedCustomExceptionsCounter = new HashMap<String, Integer>();
        this.testedNotIdentifiedExceptionsCounter = new HashMap<String, Integer>();
        this.testedAndUsedStandardOrThirdPartyExceptionsCounter = new HashMap<String, Integer>();
        this.testedAndUsedCustomExceptionsCounter = new HashMap<String, Integer>();
        this.throwsStandardOrThirdPartyExceptionsCounter = new HashMap<String, Integer>();
        this.throwsCustomExceptionsCounter = new HashMap<String, Integer>();
        this.throwsNotIdentifiedExceptionsCounter = new HashMap<String, Integer>();
        this.throwStandardOrThirdPartyExceptionsExceptionsCounter = new HashMap<String, Integer>();
        this.throwCustomExceptionsCounter = new HashMap<String, Integer>();
        this.throwNotIdentifiedExceptionsCounter = new HashMap<String, Integer>();
        this.catchCustomExceptionsCounter = new HashMap<String, Integer>();
        this.catchStandardOrThirdPartyExceptionsCounter = new HashMap<String, Integer>();
        this.catchNotIdentifiedExceptionsCounter = new HashMap<String, Integer>();
        this.customCreatedExceptions = new LinkedList<>();

        this.customExceptionsUsed = new HashMap<String, Integer>();
        this.notIdentifiedExceptionsUsed = new HashMap<String, Integer>();
        this.standardOrThirdPartyExceptionsUsed = new HashMap<String, Integer>();
        this.customExceptionsTestMethodCounter = new HashMap<String, Integer>();
        this.standardOrThirdPartyExceptionsTestMethodCounter = new HashMap<String, Integer>();
        this.notIdentifiedExceptionsTestMethodCounter = new HashMap<String, Integer>();
        this.distinctExceptionsTestMethodCounter = new HashMap<String, Integer>();

       this.adjustStringToMongoDB = adjustStringToMongoDB;
    }


    /**
     * Converte uma string comum com pontos em uma string SEM PONTOS. Isso é necessário para evitar a possibilidade
     * de haver pontos nas chaves do MongoDB, o que é proibido
     * <p>
     * Porém, essa conversão só é realizada se a flag adjustStringToMongoDB for true
     *
     * @param stringWithDots
     * @return
     */
    private String adjustStringToMongoDBRestrictions(String stringWithDots) {
        if (adjustStringToMongoDB) {
            return stringWithDots.replace(".", "#");
        }
        return stringWithDots;
    }

    /**
     * Incrementador dos mapas
     *
     * @param key
     * @param map
     */
    private void increaseMapCounter(String key, Map<String, Integer> map) {
        int currentSize;
        if (map.containsKey(key)) {
            currentSize = map.get(key);
            map.put(key, ++currentSize);
        } else {
            map.put(key, 1);
        }
    }

    private int sumAllMapValues(Map<String, Integer> map) {
        int total = 0;
        for (String key : map.keySet()) {
            total += map.get(key);
        }
        return total;
    }


    /**
     * INICIO PRELOAD DOS MAPAS
     **/
    public List<String> calculateCustomExceptionsCreatedCounter() {
        if (customCreatedExceptions.isEmpty()) {
            for (NewExceptionModel model : models.getNewExceptionModels()) {
//                if (!model.isInsideATest()) {
//                    customCreatedExceptions.add(model.getParentClassName(0));
//                }
                //even if the new exception is inside a test or test fold, add it to the NewException list
                customCreatedExceptions.add(model.getParentClassName(0));

            }
        }
        return customCreatedExceptions;
    }

    public Map<String, Integer> calculateTotalOfThrowsCustomExceptions() {
        if (throwsCustomExceptionsCounter.isEmpty()) {
            for (ThrowsExceptionModel model : models.getThrowsExceptionModels()) {
                if (!model.isInsideATest() && calculateCustomExceptionsCreatedCounter().contains(model.getException())) {
                    increaseMapCounter(model.getException(), throwsCustomExceptionsCounter);
                }
            }
        }
        return throwsCustomExceptionsCounter;
    }

    public Map<String, Integer> calculateTotalOfThrowStatementsStandardOrThirdPartyExceptions() {
        if (throwStandardOrThirdPartyExceptionsExceptionsCounter.isEmpty()) {
            for (ThrowExceptionModel model : models.getThrowExceptionModels()) {
                if (!model.isInsideATest() && !calculateCustomExceptionsCreatedCounter().contains(model.getException()) && !model.getException().equals("NOT IDENTIFIED")) {
                    increaseMapCounter(model.getException(), throwStandardOrThirdPartyExceptionsExceptionsCounter);
                }
            }
        }
        return throwStandardOrThirdPartyExceptionsExceptionsCounter;
    }

    public Map<String, Integer> calculateTotalOfThrowStatementsCustomExceptions() {
        if (throwCustomExceptionsCounter.isEmpty()) {
            for (ThrowExceptionModel model : models.getThrowExceptionModels()) {
                if (!model.isInsideATest() && calculateCustomExceptionsCreatedCounter().contains(model.getException())) {
                    increaseMapCounter(model.getException(), throwCustomExceptionsCounter);
                }
            }
        }
        return throwCustomExceptionsCounter;
    }

    public Map<String, Integer> calculateTotalOfThrowStatementsNotIdentifiedExceptions() {
        if (throwNotIdentifiedExceptionsCounter.isEmpty()) {
            for (ThrowExceptionModel model : models.getThrowExceptionModels()) {
                if (!model.isInsideATest() && model.getException().equals("NOT IDENTIFIED")) {
                    increaseMapCounter(model.getException(), throwNotIdentifiedExceptionsCounter);
                }
            }
        }
        return throwNotIdentifiedExceptionsCounter;
    }


    public Map<String, Integer> calculateTotalOfCatchStandardOrThirdPartyExceptions() {
        if (catchStandardOrThirdPartyExceptionsCounter.isEmpty()) {
            for (CatchModel model : models.getCatchModels()) {
                if (!model.isInsideATest()) {
                    for (String exception : model.getCatchedExceptions()) {
                        if (!calculateCustomExceptionsCreatedCounter().contains(exception) && !exception.equals("NOT IDENTIFIED")) {
                            increaseMapCounter(exception, catchStandardOrThirdPartyExceptionsCounter);
                        }
                    }
                }
            }
        }
        return catchStandardOrThirdPartyExceptionsCounter;
    }

    public Map<String, Integer> calculateTotalOfCatchStatementsCustomExceptions() {
        if (catchCustomExceptionsCounter.isEmpty()) {
            for (CatchModel model : models.getCatchModels()) {
                if (!model.isInsideATest()) {
                    for (String exception : model.getCatchedExceptions()) {
                        if (calculateCustomExceptionsCreatedCounter().contains(exception)) {
                            increaseMapCounter(exception, catchCustomExceptionsCounter);
                        }
                    }
                }
            }
        }
        return catchCustomExceptionsCounter;
    }

    public Map<String, Integer> calculateTotalOfCatchStatementsNotIdentifiedExceptions() {
        if (catchNotIdentifiedExceptionsCounter.isEmpty()) {
            for (CatchModel model : models.getCatchModels()) {
                if (!model.isInsideATest()) {
                    for (String exception : model.getCatchedExceptions()) {
                        if (exception.equals("NOT IDENTIFIED")) {
                            increaseMapCounter(exception, catchNotIdentifiedExceptionsCounter);
                        }
                    }
                }
            }
        }
        return catchNotIdentifiedExceptionsCounter;
    }


    /**
     * Da forma como este método e o de custom exceptions foi criado, caso haja instruções na forma
     * throws exception1, exception 2, cada exceção será contada como se fosse um throws diferente.
     *
     * @return
     */
    public Map<String, Integer> calculateTotalOfThrowsStandardOrThirdPartyExceptions() {
        if (throwsStandardOrThirdPartyExceptionsCounter.isEmpty()) {
            for (ThrowsExceptionModel model : models.getThrowsExceptionModels()) {
                if (!model.isInsideATest() && !calculateCustomExceptionsCreatedCounter().contains(model.getException()) && !model.getException().equals("NOT IDENTIFIED")) {
                    increaseMapCounter(model.getException(), throwsStandardOrThirdPartyExceptionsCounter);
                }
            }
        }
        return throwsStandardOrThirdPartyExceptionsCounter;
    }

    /**
     * Da forma como este método e o de custom exceptions foi criado, caso haja instruções na forma
     * throws exception1, exception 2, cada exceção será contada como se fosse um throws diferente.
     *
     * @return
     */
    public Map<String, Integer> calculateTotalOfThrowsNotIdentifiedExceptions() {
        if (throwsNotIdentifiedExceptionsCounter.isEmpty()) {
            for (ThrowsExceptionModel model : models.getThrowsExceptionModels()) {
                if (!model.isInsideATest() && model.getException().equals("NOT IDENTIFIED")) {
                    increaseMapCounter(model.getException(), throwsNotIdentifiedExceptionsCounter);
                }
            }
        }
        return throwsNotIdentifiedExceptionsCounter;
    }


    /**
     * Onde exceções são testadas?
     * 1 - Em @Test(Expected=Exception.class)
     * 2 - Em fail()...Catch(Exception | OutraException e )
     * 3 - Obj.expect(Exception.class) ou Obj.expect("Algum texto")
     * 4 - AssertThrow(Exception.class)
     * 5 - AssertJ
     * 6 - TestNG
     * <p>
     * Contador de uso das exceções do projeto, independente de quem as criou
     */
    public void calculateTestedExceptionsCounter() {


        if (!this.isTestedExceptionsCalculated) {

            for (JUnit_ExpectedExceptionTestAnnotationModel model : models.getExpectedExceptionAnnotationModels()) {
                String signature = model.getPackageName() + "." + model.getParentClassName().get(0) + "." + model.getParentMethodName(0);
                //signature = signature.replace("(","").replace(")","");
                signature = adjustStringToMongoDBRestrictions(signature);
                if (calculateCustomExceptionsCreatedCounter().contains(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, customExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedCustomExceptionsCounter);
                } else if (verifyIfIsAValidException(model.getExpectedExceptionName())){
                    increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedStandardOrThirdPartyExceptionsCounter);
                } else {
                    increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                    increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                }

            }

            //Validado
            for (Commom_FailCallModel model : models.getFailCallModels()) {
                if (model.getTryCatchScope().equals("catch") || model.getTryCatchScope().equals("OUTSIDE")) {
                    continue;
                }
                for (String exception : model.getCatchedExceptions()) {
                    String signature = model.getPackageName() + "." + model.getParentClassName(0) + "." + model.getParentMethodName(0);
                    signature = adjustStringToMongoDBRestrictions(signature);
                    if (calculateCustomExceptionsCreatedCounter().contains(exception)) {
                        increaseMapCounter(signature, customExceptionsTestMethodCounter);
                        increaseMapCounter(adjustStringToMongoDBRestrictions(exception), testedCustomExceptionsCounter);
                    } else if (verifyIfIsAValidException(exception)){
                        increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                        increaseMapCounter(adjustStringToMongoDBRestrictions(exception), testedStandardOrThirdPartyExceptionsCounter);
                    } else {
                        increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                        increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                    }
                }

            }

            //nem sempre dentro do .expect existe uma Excecao...
            for (JUnit_ExpectExceptionTestCallModel model : models.getExpectExceptionCallModels()) {
                String signature = model.getPackageName() + "." + model.getParentClassName(0) + "." + model.getParentMethodName(0);
                //signature = signature.replace("(","").replace(")","");
                signature = adjustStringToMongoDBRestrictions(signature);
                if (calculateCustomExceptionsCreatedCounter().contains(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, customExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedCustomExceptionsCounter);
                } else if (verifyIfIsAValidException(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedStandardOrThirdPartyExceptionsCounter);
                } else {
                    increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                    increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                }
            }

            //Validado
            for (JUnit_assertThrowsModel model : models.getJUnitassertThrowsModels()) {
                String signature = model.getPackageName() + "." + model.getParentClassName(0) + "." + model.getParentMethodName(0);
                //signature = signature.replace("(","").replace(")","");
                signature = adjustStringToMongoDBRestrictions(signature);
                if (calculateCustomExceptionsCreatedCounter().contains(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, customExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedCustomExceptionsCounter);
                } else if (verifyIfIsAValidException(model.getExpectedExceptionName())){
                    increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedStandardOrThirdPartyExceptionsCounter);
                } else {
                    increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                    increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                }
            }

            for (AssertJ_assertThatModel model : models.getAssertJAssertThatModels()) {
                String signature = model.getPackageName() + "." + model.getParentClassName(0) + "." + model.getParentMethodName(0);
                //signature = signature.replace("(","").replace(")","");
                signature = adjustStringToMongoDBRestrictions(signature);
                if (calculateCustomExceptionsCreatedCounter().contains(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, customExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedCustomExceptionsCounter);
                } else if (verifyIfIsAValidException(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedStandardOrThirdPartyExceptionsCounter);
                } else {
                    increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                    increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                }
            }

            for (AssertJ_assertThatExceptionNameModel model : models.getAssertJAssertThatExceptionNameModels()) {
                String signature = model.getPackageName() + "." + model.getParentClassName(0) + "." + model.getParentMethodName(0);
                //signature = signature.replace("(","").replace(")","");
                signature = adjustStringToMongoDBRestrictions(signature);
                if (calculateCustomExceptionsCreatedCounter().contains(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, customExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedCustomExceptionsCounter);
                } else if (verifyIfIsAValidException(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedStandardOrThirdPartyExceptionsCounter);
                } else {
                    increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                    increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                }
            }

            for (AssertJ_assertThatExceptionOfTypeModel model : models.getAssertJAssertThatExceptionOfTypeModels()) {
                String signature = model.getPackageName() + "." + model.getParentClassName(0) + "." + model.getParentMethodName(0);
                //signature = signature.replace("(","").replace(")","");
                signature = adjustStringToMongoDBRestrictions(signature);
                if (calculateCustomExceptionsCreatedCounter().contains(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, customExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedCustomExceptionsCounter);
                } else if (verifyIfIsAValidException(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedStandardOrThirdPartyExceptionsCounter);
                } else {
                    increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                    increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                }
            }

            for (AssertJ_assertThatThrownByModel model : models.getAssertJAssertThatThrownByModels()) {
                String signature = model.getPackageName() + "." + model.getParentClassName(0) + "." + model.getParentMethodName(0);
                //signature = signature.replace("(","").replace(")","");
                signature = adjustStringToMongoDBRestrictions(signature);
                if (calculateCustomExceptionsCreatedCounter().contains(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, customExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedCustomExceptionsCounter);
                } else if (verifyIfIsAValidException(model.getExpectedExceptionName())) {
                    increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                    increaseMapCounter(adjustStringToMongoDBRestrictions(model.getExpectedExceptionName()), testedStandardOrThirdPartyExceptionsCounter);
                } else {
                    increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                    increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                }
            }

            for(TestNG_ExpectedExceptionsTestAnnotationModel model : models.getTestNG_expectedExceptionsTestAnnotationModels()){
                String signature = model.getPackageName() + "." + model.getParentClassName(0) + "." + model.getParentMethodName(0);
                signature = adjustStringToMongoDBRestrictions(signature);
                for(String exceptionName : model.getExpectedExceptionsNames()){
                    if (calculateCustomExceptionsCreatedCounter().contains(exceptionName)) {
                        increaseMapCounter(signature, customExceptionsTestMethodCounter);
                        increaseMapCounter(adjustStringToMongoDBRestrictions(exceptionName), testedCustomExceptionsCounter);
                    } else if (verifyIfIsAValidException(exceptionName)) {
                        increaseMapCounter(signature, standardOrThirdPartyExceptionsTestMethodCounter);
                        increaseMapCounter(adjustStringToMongoDBRestrictions(exceptionName), testedStandardOrThirdPartyExceptionsCounter);
                    } else {
                        increaseMapCounter(signature, notIdentifiedExceptionsTestMethodCounter);
                        increaseMapCounter("NOT IDENTIFIED", testedNotIdentifiedExceptionsCounter);
                    }
                }

            }

            this.isTestedExceptionsCalculated = true;
        }

    }

    private boolean verifyIfIsAValidException(String exceptionName){
        if (exceptionName.contains("Exception") || exceptionName.contains("Error") || exceptionName.contains("Throwable")){
            return true;
        }
        return false;

    }

    public void calculateTestedAndUsedExceptionsCounter() {
        for (Map.Entry<String, Integer> entry : testedStandardOrThirdPartyExceptionsCounter.entrySet()) {
            if (standardOrThirdPartyExceptionsUsed.containsKey(entry.getKey())) {
                testedAndUsedStandardOrThirdPartyExceptionsCounter.put(entry.getKey(), entry.getValue());
            }
        }

        for (Map.Entry<String, Integer> entry : testedCustomExceptionsCounter.entrySet()) {
            if (customExceptionsUsed.containsKey(entry.getKey())) {
                testedAndUsedCustomExceptionsCounter.put(entry.getKey(), entry.getValue());
            }
        }

    }


    /** FIM PRELOAD DOS MAPAS **/

    /**
     * Contador de exceções usadas e que não foram identificadas
     * Este número é calculado a partir da soma dos seguintes parametros:
     * num# de throws,
     * num# de throw,
     * num# de catchs,
     *
     * @return
     */
    public int getTotalNumberOfDistinctUsedNotIdentifiedExceptions() {
        if (totalNumberOfDistinctUsedNotIdentifiedExceptions != null) {
            return totalNumberOfDistinctUsedNotIdentifiedExceptions;
        }

        mergeMap(calculateTotalOfThrowsNotIdentifiedExceptions(), notIdentifiedExceptionsUsed);
        //o Throw possui diversas particularidades que ainda precisam ser tratadas
        mergeMap(calculateTotalOfThrowStatementsNotIdentifiedExceptions(), notIdentifiedExceptionsUsed);
        //catchs
        mergeMap(calculateTotalOfCatchStatementsNotIdentifiedExceptions(), notIdentifiedExceptionsUsed);
        //System.out.println(exceptionList);

        return notIdentifiedExceptionsUsed.size();
    }

    /**
     * Contador de quantas vezes as exceções não identificadas foram usadas
     * Este número é calculado a partir da soma dos seguintes parametros:
     * num# de throws,
     * num# de throw,
     * num# de catchs,
     *
     * @return
     */
    public int getTotalNumberOfNotIdentifiedExceptionsUses() {
        if (totalNumberOfNotIdentifiedExceptionsUses != null) {
            return totalNumberOfNotIdentifiedExceptionsUses;
        }
        int total = 0;
        total += this.getTotalNumberOfThrowsStatementNotIdentifiedExceptions();
        total += this.getTotalNumberOfThrowStatementNotIdentifiedExceptions();
        total += this.getTotalNumberOfCatchStatementNotIdentifiedExceptions();


        return total;
    }

    /**
     * Contador de quantas vezes as exceções não identificadas foram usadas
     * Este número é calculado a partir da soma dos seguintes parametros:
     * num# de throws,
     * num# de throw,
     * num# de catchs,
     *
     * @return
     */
    public int getTotalNumberOfCustomExceptionsUses() {
        if (totalNumberOfCustomExceptionsUses != null) {
            return totalNumberOfCustomExceptionsUses;
        }
        int total = 0;
        total += this.getTotalNumberOfThrowsStatementCustomExceptions();
        total += this.getTotalNumberOfThrowStatementCustomExceptions();
        total += this.getTotalNumberOfCatchStatementCustomExceptions();


        return total;
    }

    /**
     * Contador de quantas vezes as exceções não identificadas foram usadas
     * Este número é calculado a partir da soma dos seguintes parametros:
     * num# de throws,
     * num# de throw,
     * num# de catchs,
     *
     * @return
     */
    public int getTotalNumberOfStandardOrThirdPartyExceptionsUses() {
        if (totalNumberOfStandardOrThirdPartyExceptionsUses != null) {
            return totalNumberOfStandardOrThirdPartyExceptionsUses;
        }
        int total = 0;
        total += this.getTotalNumberOfThrowsStatementsStandardOrThirdPartyExceptions();
        total += this.getTotalNumberOfThrowStatementsStandardOrThirdPartyExceptions();
        total += this.getTotalNumberOfCatchStatementsStandardOrThirdPartyExceptions();


        return total;
    }

    /**
     * Contador de quantas vezes as exceções não identificadas foram usadas
     * Este número é calculado a partir da soma dos seguintes parametros:
     * num# de throws,
     * num# de throw,
     * num# de catchs,
     *
     * @return
     */
    public int getTotalNumberOfExceptionsUses() {
        if (totalNumberOfExceptionsUses != null) {
            return totalNumberOfExceptionsUses;
        }
        int total = 0;
        total += this.getTotalNumberOfStandardOrThirdPartyExceptionsUses();
        total += this.getTotalNumberOfCustomExceptionsUses();
        total += this.getTotalNumberOfNotIdentifiedExceptionsUses();


        return total;
    }

    /**
     * Contador de exceções usadas e que foram criadas em outros projetos
     * Este número é calculado a partir da soma dos seguintes parametros:
     * num# de throws,
     * num# de throw,
     * num# de catchs,
     *
     * @return
     */
    public int getTotalNumberOfDistinctUsedStandardOrThirdPartyExceptions() {
        if (totalNumberOfDistinctUsedStandardOrThirdPartyExceptions != null) {
            return totalNumberOfDistinctUsedStandardOrThirdPartyExceptions;
        }

        mergeMap(calculateTotalOfThrowsStandardOrThirdPartyExceptions(), standardOrThirdPartyExceptionsUsed);
        //o Throw possui diversas particularidades que ainda precisam ser tratadas
        mergeMap(calculateTotalOfThrowStatementsStandardOrThirdPartyExceptions(), standardOrThirdPartyExceptionsUsed);
        //catchs
        mergeMap(calculateTotalOfCatchStandardOrThirdPartyExceptions(), standardOrThirdPartyExceptionsUsed);
        //System.out.println(exceptionList);
        return standardOrThirdPartyExceptionsUsed.size();
    }

    /**
     * Contador de exceções usadas e que foram criadas no projeto alvo
     * Este número é calculado a partir da soma dos seguintes parametros:
     * num# de throws,
     * num# de throw,
     * num# de catchs,
     *
     * @return
     */
    public int getTotalNumberOfDistinctUsedCustomExceptions() {
        if (totalNumberOfDistinctUsedCustomExceptions != null) {
            return totalNumberOfDistinctUsedCustomExceptions;
        }
        mergeMap(calculateTotalOfThrowsCustomExceptions(), customExceptionsUsed);
        //o Throw possui diversas particularidades que ainda precisam ser tratadas
        mergeMap(calculateTotalOfThrowStatementsCustomExceptions(), customExceptionsUsed);
        //catchs
        mergeMap(calculateTotalOfCatchStatementsCustomExceptions(), customExceptionsUsed);
        //System.out.println(exceptionList);
        return customExceptionsUsed.size();
    }


    public int getTotalNumberOfCreatedCustomExceptions() {
        if (totalNumberOfCreatedCustomExceptions != null) {
            return totalNumberOfCreatedCustomExceptions;
        }
        return calculateCustomExceptionsCreatedCounter().size();
    }

    public int getTestNG_totalNumberOfExpectedExceptionsAttribute() {
        if (testNG_totalNumberOfExpectedExceptionsAttribute != null) {
            return testNG_totalNumberOfExpectedExceptionsAttribute;
        }
        return models.getTestNG_expectedExceptionsTestAnnotationModels().size();
    }

    public int getJunit_totalNumberOfExpectedAttribute() {
        if (junit_totalNumberOfExpectedAttribute != null) {
            return junit_totalNumberOfExpectedAttribute;
        }
        return models.getExpectedExceptionAnnotationModels().size();
    }

    public int getJunit_totalNumberOfExpectCalls() {
        if (junit_totalNumberOfExpectCalls != null) {
            return junit_totalNumberOfExpectCalls;
        }
        return models.getExpectExceptionCallModels().size();
    }


    /**
     * Calcula o numero de METODOS que fazem alguma verificacao de excecoes stanrdard/ThirdParty
     *
     * @return
     */
    public int getTotalNumberOfStandardOrThirdPartyExceptionsTestMethods() {
        if (totalNumberOfStandardOrThirdPartyExceptionsTestMethods != null) {
            return totalNumberOfStandardOrThirdPartyExceptionsTestMethods;
        }
        //calculateTestedExceptionsCounter();
        return standardOrThirdPartyExceptionsTestMethodCounter.size();
    }

    /**
     * Calcula o numero de METODOS que fazem alguma verificacao de excecoes do proprio projeto
     *
     * @return
     */
    public int getTotalNumberOfCustomExceptionsTestMethods() {
        if (totalNumberOfCustomExceptionsTestMethods != null) {
            return totalNumberOfCustomExceptionsTestMethods;
        }
        //calculateTestedExceptionsCounter();
        return customExceptionsTestMethodCounter.size();
    }

    /**
     * @return
     */
    public int getTotalNumberOfNotIdentifiedExceptionsTestMethods() {
        if (totalNumberOfNotIdentifiedExceptionsTestMethods != null) {
            return totalNumberOfNotIdentifiedExceptionsTestMethods;
        }
        //calculateTestedExceptionsCounter();
        return notIdentifiedExceptionsTestMethodCounter.size();
    }

    public int getTotalNumberOfAllTestedStandardOrThirdPartyExceptions() {
        if (totalNumberOfAllTestedStandardOrThirdPartyExceptions != null) {
            return totalNumberOfAllTestedStandardOrThirdPartyExceptions;
        }
        //calculateTestedExceptionsCounter();
        return testedStandardOrThirdPartyExceptionsCounter.size();
    }

    public int getTotalNumberOfDistinctTestedStandardOrThirdPartyExceptions() {
        if (totalNumberOfDistinctTestedStandardOrThirdPartyExceptions != null) {
            return totalNumberOfDistinctTestedStandardOrThirdPartyExceptions;
        }
        calculateTestedAndUsedExceptionsCounter();
        return testedAndUsedStandardOrThirdPartyExceptionsCounter.size();
    }

    public int getTotalNumberOfAllTestedNotIdentifiedExceptions() {
        if (totalNumberOfAllTestedNotIdentifiedExceptions != null) {
            return totalNumberOfAllTestedNotIdentifiedExceptions;
        }
        calculateTestedAndUsedExceptionsCounter();
        return testedNotIdentifiedExceptionsCounter.size();
    }


    public int getTotalNumberOfAllTestedCustomExceptions() {
        if (totalNumberOfAllTestedCustomExceptions != null) {
            return totalNumberOfAllTestedCustomExceptions;
        }
        //calculateTestedExceptionsCounter();
        return testedCustomExceptionsCounter.size();
    }

    public int getTotalNumberOfDistinctTestedCustomExceptions() {
        if (totalNumberOfDistinctTestedCustomExceptions != null) {
            return totalNumberOfDistinctTestedCustomExceptions;
        }
        calculateTestedAndUsedExceptionsCounter();
        return testedAndUsedCustomExceptionsCounter.size();
    }

    public int getTotalNumberOfDistinctTestedExceptions() {
        if (totalNumberOfDistinctTestedExceptions != null) {
            return totalNumberOfDistinctTestedExceptions;
        }
        calculateTestedAndUsedExceptionsCounter();
        return testedAndUsedCustomExceptionsCounter.size() + testedAndUsedStandardOrThirdPartyExceptionsCounter.size();
    }


    public int getTotalNumberOfTestMethods() {
        if (totalNumberOfTestMethods != null) {
            return totalNumberOfTestMethods;
        }
        return models.getOrdinaryAnnotationTestModels().size() + models.getOrdinaryExtendedTestModels().size();
    }

    public int getCommom_totalNumberOfFailCalls() {
        if (commom_totalNumberOfFailCalls != null) {
            return commom_totalNumberOfFailCalls;
        }
        return models.getFailCallModels().size();
    }

    public int getCommom_totalNumberOfFailInsideCatchScope() {
        if (commom_totalNumberOfFailInsideCatchScope != null) {
            return commom_totalNumberOfFailInsideCatchScope;
        }
        int counter = 0;
        for (Commom_FailCallModel md : models.getFailCallModels()) {
            if (md.getTryCatchScope().equals("catch")) {
                counter++;
            }
        }
        return counter;
    }

    public int getCommom_totalNumberOfFailInsideTryScope() {
        if (commom_totalNumberOfFailInsideTryScope != null) {
            return commom_totalNumberOfFailInsideTryScope;
        }
        int counter = 0;
        for (Commom_FailCallModel md : models.getFailCallModels()) {
            if (md.getTryCatchScope().equals("try")) {
                counter++;
            }
        }
        return counter;
    }

    public int getCommom_totalNumberOfFailOutsideTryCatchScope() {
        if (commom_totalNumberOfFailOutsideTryCatchScope != null) {
            return commom_totalNumberOfFailOutsideTryCatchScope;
        }
        int counter = 0;
        for (Commom_FailCallModel md : models.getFailCallModels()) {
            if (md.getTryCatchScope().equals("OUTSIDE")) {
                counter++;
            }
        }
        return counter;
    }

    public int getJunit_totalNumberOfAssertThrows() {
        if (junit_totalNumberOfAssertThrows != null) {
            return junit_totalNumberOfAssertThrows;
        }
        return models.getJUnitassertThrowsModels().size();
    }

    public int getAssertj_totalNumberOfAsserts() {
        if (assertj_totalNumberOfAsserts != null) {
            return assertj_totalNumberOfAsserts;
        }
        return models.getAssertJAssertThatExceptionOfTypeModels().size()
                + models.getAssertJAssertThatThrownByModels().size()
                + models.getAssertJAssertThatExceptionNameModels().size()
                + models.getAssertJAssertThatModels().size();
    }

    public int getAssertj_totalNumberOfAssertThat() {
        if (assertj_totalNumberOfAssertThat != null) {
            return assertj_totalNumberOfAssertThat;

        }
        return models.getAssertJAssertThatModels().size();
    }

    public int getAssertj_totalNumberOfAssertThatExceptionName() {
        if (assertj_totalNumberOfAssertThatExceptionName != null) {
            return assertj_totalNumberOfAssertThatExceptionName;

        }
        return models.getAssertJAssertThatExceptionNameModels().size();
    }

    public int getAssertj_totalNumberOfAssertThatExceptionOfType() {
        if (assertj_totalNumberOfAssertThatExceptionOfType != null) {
            return assertj_totalNumberOfAssertThatExceptionOfType;

        }
        return models.getAssertJAssertThatExceptionOfTypeModels().size();
    }

    public int getAssertj_totalNumberOfAssertThatThrownBy() {
        if (assertj_totalNumberOfAssertThatThrownBy != null) {
            return assertj_totalNumberOfAssertThatThrownBy;

        }
        return models.getAssertJAssertThatThrownByModels().size();
    }

    public int getTotalNumberOfThrowStatementCustomExceptions() {
        if (totalNumberOfThrowStatementCustomExceptions != null) {
            return totalNumberOfThrowStatementCustomExceptions;
        }
        return sumAllMapValues(calculateTotalOfThrowStatementsCustomExceptions());
    }

    public int getTotalNumberOfThrowStatementsStandardOrThirdPartyExceptions() {
        if (totalNumberOfThrowStatementsStandardOrThirdPartyExceptions != null) {
            return totalNumberOfThrowStatementsStandardOrThirdPartyExceptions;
        }
        return sumAllMapValues(calculateTotalOfThrowStatementsStandardOrThirdPartyExceptions());
    }

    public int getTotalNumberOfThrowStatementsNotIdentifiedExceptions() {
        if (totalNumberOfThrowStatementNotIdentifiedExceptions != null) {
            return totalNumberOfThrowStatementNotIdentifiedExceptions;
        }
        return sumAllMapValues(calculateTotalOfThrowStatementsNotIdentifiedExceptions());
    }

    public int getTotalNumberOfThrowStatements() {
        if (totalNumberOfThrowStatements != null) {
            return totalNumberOfThrowStatements;
        }
        return getTotalNumberOfThrowStatementCustomExceptions() +
                getTotalNumberOfThrowStatementsStandardOrThirdPartyExceptions() +
                getTotalNumberOfThrowStatementsNotIdentifiedExceptions();
    }


    public int getTotalNumberOfThrowsStatementCustomExceptions() {
        if (totalNumberOfThrowsStatementCustomExceptions != null) {
            return totalNumberOfThrowsStatementCustomExceptions;
        }
        return sumAllMapValues(calculateTotalOfThrowsCustomExceptions());
    }

    public int getTotalNumberOfThrowsStatementNotIdentifiedExceptions() {
        if (totalNumberOfThrowsStatementNotIdentifiedExceptions != null) {
            return totalNumberOfThrowsStatementNotIdentifiedExceptions;
        }
        return sumAllMapValues(calculateTotalOfThrowsNotIdentifiedExceptions());
    }

    public int getTotalNumberOfThrowsStatementsStandardOrThirdPartyExceptions() {
        if (totalNumberOfThrowsStatementsStandardOrThirdPartyExceptions != null) {
            return totalNumberOfThrowsStatementsStandardOrThirdPartyExceptions;
        }
        return sumAllMapValues(calculateTotalOfThrowsStandardOrThirdPartyExceptions());
    }

    public int getTotalNumberOfThrowsStatements() {
        if (totalNumberOfThrowsStatements != null) {
            return totalNumberOfThrowsStatements;
        }
        return getTotalNumberOfThrowsStatementCustomExceptions() +
                getTotalNumberOfThrowsStatementsStandardOrThirdPartyExceptions() +
                getTotalNumberOfThrowsStatementNotIdentifiedExceptions();
    }

    public int getTotalNumberOfCatchStatementCustomExceptions() {
        if (totalNumberOfCatchStatementCustomExceptions != null) {
            return totalNumberOfCatchStatementCustomExceptions;
        }
        return sumAllMapValues(calculateTotalOfCatchStatementsCustomExceptions());
    }

    public int getTotalNumberOfCatchStatementsStandardOrThirdPartyExceptions() {
        if (totalNumberOfCatchStatementsStandardOrThirdPartyExceptions != null) {
            return totalNumberOfCatchStatementsStandardOrThirdPartyExceptions;
        }
        return sumAllMapValues(calculateTotalOfCatchStandardOrThirdPartyExceptions());
    }

    public int getTotalNumberOfCatchStatementsNotIdentifiedExceptions() {
        if (totalNumberOfCatchStatementNotIdentifiedExceptions != null) {
            return totalNumberOfCatchStatementNotIdentifiedExceptions;
        }
        return sumAllMapValues(calculateTotalOfCatchStatementsNotIdentifiedExceptions());
    }

    public int getTotalNumberOfCatchStatements() {
        if (totalNumberOfCatchStatements != null) {
            return totalNumberOfCatchStatements;
        }
        return getTotalNumberOfCatchStatementCustomExceptions() +
                getTotalNumberOfCatchStatementsStandardOrThirdPartyExceptions() +
                getTotalNumberOfCatchStatementsNotIdentifiedExceptions();
    }


    public int getTotalNumberOfTestsByAnnotation() {
        if (this.totalNumberOfTestsByAnnotation != null) {
            return totalNumberOfTestsByAnnotation;
        }
        return this.models.getOrdinaryAnnotationTestModels().size();
    }

    public int getTotalNumberOfTestsByInheritance() {
        if (this.totalNumberOfTestsByInheritance != null) {
            return totalNumberOfTestsByInheritance;
        }
        return this.models.getOrdinaryExtendedTestModels().size();
    }

    public int getTotalNumberOfTestsWithJUnitImports() {
        if (!isFrameworksCounted) {
            countTestsByFramework();
        }
        return totalNumberOfTestsWithJUnitImports;
    }

    public int getTotalNumberOfTestsWithTestNGImports() {
        if (!isFrameworksCounted) {
            countTestsByFramework();
        }
        return totalNumberOfTestsWithTestNGImports;
    }

    public int getTotalNumberOfTestsWithNotIdentifiedImports() {
        if (!isFrameworksCounted) {
            countTestsByFramework();
        }
        return totalNumberOfTestsWithNotIdentifiedImports;
    }

    private void countTestsByFramework(){
        if(isFrameworksCounted){
            return;
        }
        int jUnit = 0;
        int testNg = 0;
        int assertJ = 0;
        int others = 0;
        for(OrdinaryExtendedTestModel model : this.models.getOrdinaryExtendedTestModels()){
            if(model.isInsideAJUnitTest()) {
                jUnit++;
            }
            if (model.isInsideATestNGTest()) {
                testNg++;
            }
            if (model.isInsideAAssertJTest()) {
                assertJ++;
            }
            if (jUnit + testNg + assertJ == 0){
                others++;
            }
        }
        for(OrdinaryAnnotationTestModel model : this.models.getOrdinaryAnnotationTestModels()){
            if(model.isInsideAJUnitTest()) {
                jUnit++;
            }
            if (model.isInsideATestNGTest()) {
                testNg++;
            }
            if (model.isInsideAAssertJTest()) {
                assertJ++;
            }
            if (jUnit + testNg + assertJ == 0){
                others++;
            }
        }
        this.totalNumberOfTestsWithJUnitImports = jUnit;
        this.totalNumberOfTestsWithAssertJImports = assertJ;
        this.totalNumberOfTestsWithTestNGImports = testNg;
        this.totalNumberOfTestsWithNotIdentifiedImports = others;
        this.isFrameworksCounted = true;

    }


    public void calculateStatistics() {
        this.totalNumberOfTestsByAnnotation = this.getTotalNumberOfTestsByAnnotation();
        this.totalNumberOfTestsByInheritance = this.getTotalNumberOfTestsByInheritance();
        this.totalNumberOfTestMethods = this.getTotalNumberOfTestMethods();
        this.totalNumberOfCreatedCustomExceptions = this.getTotalNumberOfCreatedCustomExceptions();
        /*** Hybrid ***/
        this.commom_totalNumberOfFailInsideCatchScope = this.getCommom_totalNumberOfFailInsideCatchScope();
        this.commom_totalNumberOfFailInsideTryScope = this.getCommom_totalNumberOfFailInsideTryScope();
        this.commom_totalNumberOfFailOutsideTryCatchScope = this.getCommom_totalNumberOfFailOutsideTryCatchScope();
        this.commom_totalNumberOfFailCalls = this.getCommom_totalNumberOfFailCalls();
        /*** JUnit ***/
        this.junit_totalNumberOfExpectedAttribute = this.getJunit_totalNumberOfExpectedAttribute();
        this.junit_totalNumberOfExpectCalls = this.getJunit_totalNumberOfExpectCalls();
        this.junit_totalNumberOfAssertThrows = this.getJunit_totalNumberOfAssertThrows();
        /*** AssertJ ***/
        this.assertj_totalNumberOfAsserts = this.getAssertj_totalNumberOfAsserts();
        this.assertj_totalNumberOfAssertThatThrownBy = this.getAssertj_totalNumberOfAssertThatThrownBy();
        this.assertj_totalNumberOfAssertThatExceptionOfType = this.getAssertj_totalNumberOfAssertThatExceptionOfType();
        this.assertj_totalNumberOfAssertThat = this.getAssertj_totalNumberOfAssertThat();
        this.assertj_totalNumberOfAssertThatExceptionName = this.getAssertj_totalNumberOfAssertThatExceptionName();
        /*** TestNG ***/
        this.testNG_totalNumberOfExpectedExceptionsAttribute = this.getTestNG_totalNumberOfExpectedExceptionsAttribute();

        /*** SUT ***/
        this.totalNumberOfThrowsStatementsStandardOrThirdPartyExceptions = this.getTotalNumberOfThrowsStatementsStandardOrThirdPartyExceptions();
        this.totalNumberOfThrowsStatementCustomExceptions = this.getTotalNumberOfThrowsStatementCustomExceptions();
        this.totalNumberOfThrowsStatementNotIdentifiedExceptions = this.getTotalNumberOfThrowsStatementNotIdentifiedExceptions();
        this.totalNumberOfThrowsStatements = this.getTotalNumberOfThrowsStatements();
        this.totalNumberOfThrowStatementsStandardOrThirdPartyExceptions = this.getTotalNumberOfThrowStatementsStandardOrThirdPartyExceptions();
        this.totalNumberOfThrowStatementCustomExceptions = this.getTotalNumberOfThrowStatementCustomExceptions();
        this.totalNumberOfThrowStatementNotIdentifiedExceptions = this.getTotalNumberOfThrowStatementsNotIdentifiedExceptions();
        this.totalNumberOfThrowStatements = this.getTotalNumberOfThrowStatements();
        this.totalNumberOfCatchStatementsStandardOrThirdPartyExceptions = this.getTotalNumberOfCatchStatementsStandardOrThirdPartyExceptions();
        this.totalNumberOfCatchStatementCustomExceptions = this.getTotalNumberOfCatchStatementCustomExceptions();
        this.totalNumberOfCatchStatementNotIdentifiedExceptions = this.getTotalNumberOfCatchStatementsNotIdentifiedExceptions();
        this.totalNumberOfCatchStatements = this.getTotalNumberOfCatchStatements();
        this.totalNumberOfDistinctUsedCustomExceptions = this.getTotalNumberOfDistinctUsedCustomExceptions();
        this.totalNumberOfDistinctUsedNotIdentifiedExceptions = this.getTotalNumberOfDistinctUsedNotIdentifiedExceptions();
        this.totalNumberOfDistinctUsedStandardOrThirdPartyExceptions = this.getTotalNumberOfDistinctUsedStandardOrThirdPartyExceptions();
        this.totalNumberOfDistinctUsedExceptions = this.getTotalNumberOfDistinctUsedExceptions();
        //uses
        this.totalNumberOfCustomExceptionsUses = this.getTotalNumberOfCustomExceptionsUses();
        this.totalNumberOfStandardOrThirdPartyExceptionsUses = this.getTotalNumberOfStandardOrThirdPartyExceptionsUses();
        this.totalNumberOfNotIdentifiedExceptionsUses = this.getTotalNumberOfNotIdentifiedExceptionsUses();
        this.totalNumberOfExceptionsUses = this.getTotalNumberOfExceptionsUses();


        //Lembrando que um metodo pode testar, ao mesmo tempo, uma excecao custom e uma standard/ThirdParty.
        //Nos casos que isso ocorre, o metodo sera contado 1,2,3...n de acordo com a quantidade de excecoes verificadas
        calculateTestedExceptionsCounter();
        //used and tested
        this.totalNumberOfDistinctTestedStandardOrThirdPartyExceptions = this.getTotalNumberOfDistinctTestedStandardOrThirdPartyExceptions();
        this.totalNumberOfDistinctTestedCustomExceptions = this.getTotalNumberOfDistinctTestedCustomExceptions();
        this.totalNumberOfDistinctTestedExceptions = this.getTotalNumberOfDistinctTestedExceptions();

        this.totalNumberOfCustomExceptionsTestMethods = this.getTotalNumberOfCustomExceptionsTestMethods();
        this.totalNumberOfStandardOrThirdPartyExceptionsTestMethods = this.getTotalNumberOfStandardOrThirdPartyExceptionsTestMethods();
        this.totalNumberOfNotIdentifiedExceptionsTestMethods = this.getTotalNumberOfNotIdentifiedExceptionsTestMethods();
        this.totalNumberOfExceptionTests = this.getTotalNumberOfExceptionTests();
        this.totalNumberOfExceptionalBehaviorTestMethods = this.getTotalNumberOfExceptionalBehaviorTestMethods();

        this.totalNumberOfAllTestedStandardOrThirdPartyExceptions = this.getTotalNumberOfAllTestedStandardOrThirdPartyExceptions();
        this.totalNumberOfAllTestedNotIdentifiedExceptions = this.getTotalNumberOfAllTestedNotIdentifiedExceptions();
        this.totalNumberOfAllTestedCustomExceptions = this.getTotalNumberOfAllTestedCustomExceptions();
        this.totalNumberOfAllTestedExceptions = this.totalNumberOfAllTestedStandardOrThirdPartyExceptions  + this.totalNumberOfAllTestedCustomExceptions;

        //Count how many tests uses each framework
        countTestsByFramework();

        //calculatePercentages();

    }

    private void calculatePercentages() {
        //Calculo a nivel de metodo (ABSOLUTO -- Em Relacao ao total geral)
        this.totalAbsolutPercentegeOfNotIdentifiedExceptionsTestMethods = this.getTotalAbsolutPercentageOfNotIdentifiedExceptionsTestMethods();
        this.totalAbsolutPercentegeOfStandardOrThirdPartyExceptionsTestMethods = this.getTotalAbsolutPercentageOfStandardOrThirdPartyExceptionsTestMethods();
        this.totalAbsolutPercentageCustomExceptionsTestMethods = this.getTotalAbsolutPercentageCustomExceptionsTestMethods();
        this.totalAbsolutPercentageExceptionsTestMethods = this.getTotalAbsolutPercentageExceptionsTestMethods();

        //Calculo a nivel de excecao (ABSOLUTO - Em relacao ao total Geral)
        this.totalAbsolutPercentegeOfNotIdentifiedExceptionsTested = this.getTotalAbsolutPercentageOfNotIdentifiedExceptionsTested();
        this.totalAbsolutPercentegeOfStandardOrThirdPartyExceptionsTested = this.getTotalAbsolutPercentageOfStandardOrThirdPartyExceptionsTested();
        this.totalAbsolutPercentageCustomExceptionsTested = this.getTotalAbsolutPercentageCustomExceptionsTested();
        this.totalAbsolutPercentageExceptionsTested = this.getTotalAbsolutPercentageExceptionsTested();

        //Calculo a nivel de excecao (Relativo - Em relacao ao total de cada tipo)
        this.totalRelativePercentegeOfNotIdentifiedExceptionsTested = this.getTotalRelativePercentageOfNotIdentifiedExceptionsTested();
        this.totalRelativePercentegeOfStandardOrThirdPartyExceptionsTested = this.getTotalRelativePercentageOfStandardOrThirdPartyExceptionsTested();
        this.totalRelativePercentageCustomExceptionsTested = this.getTotalRelativePercentageCustomExceptionsTested();
    }


    /*** OS MÉTODOS ABAIXO DESCONSIDERAM AS EXCECOES NAO IDENTIFICADAS!!!! ***/

    /**
     * Desconsidera o numero de excecoes nao identificadas
     *
     * @return
     */
    public double getTotalAbsolutPercentageExceptionsTested() {
        if (this.getTotalNumberOfDistinctUsedExceptions() == 0) {
            return 0;
        }
        double value = ((this.getTotalNumberOfAllTestedCustomExceptions() +
                this.getTotalNumberOfAllTestedStandardOrThirdPartyExceptions()) / (double) this.getTotalNumberOfDistinctUsedExceptions());
        return formatDecimalOutput(value);
    }


    public double getTotalAbsolutPercentageOfStandardOrThirdPartyExceptionsTested() {
        if (this.getTotalNumberOfDistinctUsedExceptions() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfAllTestedStandardOrThirdPartyExceptions() / (double) this.getTotalNumberOfDistinctUsedExceptions();
        return formatDecimalOutput(value);
    }

    /**
     * Neste caso, o numero de exceções usadas é acrescido do numero de execoes nao identificadas usadas
     *
     * @return
     */
    public double getTotalAbsolutPercentageOfNotIdentifiedExceptionsTested() {
        if (this.getTotalNumberOfDistinctUsedExceptions() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfAllTestedNotIdentifiedExceptions() / ((double) this.getTotalNumberOfDistinctUsedExceptions() +
                this.getTotalNumberOfDistinctUsedNotIdentifiedExceptions());
        return formatDecimalOutput(value);
    }

    /**
     * Desconsidera o numero de excecoes nao identificadas
     *
     * @return
     */
    public double getTotalRelativePercentageOfStandardOrThirdPartyExceptionsTested() {
        if (this.getTotalNumberOfDistinctUsedStandardOrThirdPartyExceptions() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfAllTestedStandardOrThirdPartyExceptions() / (double) this.getTotalNumberOfDistinctUsedStandardOrThirdPartyExceptions();
        return formatDecimalOutput(value);
    }

    public double getTotalRelativePercentageOfNotIdentifiedExceptionsTested() {
        if (this.getTotalNumberOfDistinctUsedNotIdentifiedExceptions() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfAllTestedNotIdentifiedExceptions() / (double) this.getTotalNumberOfDistinctUsedNotIdentifiedExceptions();
        return formatDecimalOutput(value);
    }

    public double getTotalRelativePercentageCustomExceptionsTested() {
        if (this.getTotalNumberOfDistinctUsedCustomExceptions() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfAllTestedCustomExceptions() / (double) this.getTotalNumberOfDistinctUsedCustomExceptions();
        return formatDecimalOutput(value);
    }

    /**
     * Desconsidera o numero de excecoes nao identificadas
     *
     * @return
     */
    public double getTotalAbsolutPercentageCustomExceptionsTested() {
        if (this.getTotalNumberOfDistinctUsedExceptions() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfAllTestedCustomExceptions() / (double) this.getTotalNumberOfDistinctUsedExceptions();
        return formatDecimalOutput(value);
    }

    /**
     * CONSIDERA o numero de excecoes nao identificadas
     *
     * @return
     */
    public double getTotalAbsolutPercentageExceptionsTestMethods() {
        if (this.getTotalNumberOfTestMethods() == 0) {
            return 0;
        }

        double value = ((this.getTotalNumberOfStandardOrThirdPartyExceptionsTestMethods() +
                this.getTotalNumberOfCustomExceptionsTestMethods() +
                this.getTotalNumberOfNotIdentifiedExceptionsTestMethods()) / (double) this.getTotalNumberOfTestMethods());
        return formatDecimalOutput(value);
    }

    private double formatDecimalOutput(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    /**
     * CONSIDERA o numero de excecoes nao identificadas
     *
     * @return
     */
    public double getTotalAbsolutPercentageCustomExceptionsTestMethods() {
        if (this.getTotalNumberOfTestMethods() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfCustomExceptionsTestMethods() / (double) this.getTotalNumberOfTestMethods();
        return formatDecimalOutput(value);
    }

    /**
     * CONSIDERA o numero de excecoes nao identificadas
     *
     * @return
     */
    public double getTotalAbsolutPercentageOfStandardOrThirdPartyExceptionsTestMethods() {
        if (this.getTotalNumberOfTestMethods() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfStandardOrThirdPartyExceptionsTestMethods() / (double) this.getTotalNumberOfTestMethods();
        return formatDecimalOutput(value);
    }

    /**
     * CONSIDERA o numero de excecoes nao identificadas
     *
     * @return
     */
    public double getTotalAbsolutPercentageOfNotIdentifiedExceptionsTestMethods() {
        if (this.getTotalNumberOfTestMethods() == 0) {
            return 0;
        }
        double value = this.getTotalNumberOfNotIdentifiedExceptionsTestMethods() / (double) this.getTotalNumberOfTestMethods();
        return formatDecimalOutput(value);
    }

    /**
     * CONSIDERA o numero de excecoes nao identificadas
     *
     * @return
     */
    public int getTotalNumberOfExceptionTests() {
        return this.getTotalNumberOfStandardOrThirdPartyExceptionsTestMethods() +
                this.getTotalNumberOfCustomExceptionsTestMethods() +
                this.getTotalNumberOfNotIdentifiedExceptionsTestMethods();

    }

    /**
     * CONSIDERA o numero de excecoes nao identificadas e conta uma unica vez cada metodo
     *
     * @return
     */
    public int getTotalNumberOfExceptionalBehaviorTestMethods() {
        mergeMap(getStandardOrThirdPartyExceptionsTestMethodCounter(), distinctExceptionsTestMethodCounter);
        mergeMap(getCustomExceptionsTestMethodCounter(), distinctExceptionsTestMethodCounter);
        mergeMap(getNotIdentifiedExceptionsTestMethodCounter(), distinctExceptionsTestMethodCounter);
        return distinctExceptionsTestMethodCounter.size();

    }

    /**
     * DESCONSIDERA o numero de excecoes nao identificadas
     *
     * @return
     */
    public int getTotalNumberOfDistinctUsedExceptions() {
        return this.getTotalNumberOfDistinctUsedCustomExceptions() +
                this.getTotalNumberOfDistinctUsedStandardOrThirdPartyExceptions();
    }

    public void mergeMap(Map<String, Integer> fromMap, Map<String, Integer> toMap) {
        for (Map.Entry<String, Integer> entry : fromMap.entrySet()) {
            if (toMap.containsKey(entry.getKey())) {
                toMap.put(entry.getKey(), entry.getValue() + toMap.get(entry.getKey()));
            } else {

                toMap.put(entry.getKey(), entry.getValue());
            }
        }
    }


    public Map<String, Integer> getTestedStandardOrThirdPartyExceptionsCounter() {
        return testedStandardOrThirdPartyExceptionsCounter;
    }

    public Map<String, Integer> getTestedCustomExceptionsCounter() {
        return testedCustomExceptionsCounter;
    }

    public Map<String, Integer> getThrowsStandardOrThirdPartyExceptionsCounter() {
        return throwsStandardOrThirdPartyExceptionsCounter;
    }

    public Map<String, Integer> getThrowsCustomExceptionsCounter() {
        return throwsCustomExceptionsCounter;
    }

    public Map<String, Integer> getThrowStandardOrThirdPartyExceptionsExceptionsCounter() {
        return throwStandardOrThirdPartyExceptionsExceptionsCounter;
    }

    public Map<String, Integer> getThrowCustomExceptionsCounter() {
        return throwCustomExceptionsCounter;
    }

    public List<String> getCustomCreatedExceptions() {
        return customCreatedExceptions;
    }

    public Map<String, Integer> getCatchStandardOrThirdPartyExceptionsCounter() {
        return catchStandardOrThirdPartyExceptionsCounter;
    }

    public Map<String, Integer> getCatchCustomExceptionsCounter() {
        return catchCustomExceptionsCounter;
    }

    public Map<String, Integer> getStandardOrThirdPartyExceptionsUsed() {
        return standardOrThirdPartyExceptionsUsed;
    }

    public Map<String, Integer> getCustomExceptionsUsed() {
        return customExceptionsUsed;
    }

    public Map<String, Integer> getThrowNotIdentifiedExceptionsCounter() {
        return throwNotIdentifiedExceptionsCounter;
    }

    public Map<String, Integer> getTestedNotIdentifiedExceptionsCounter() {
        return testedNotIdentifiedExceptionsCounter;
    }

    public Map<String, Integer> getCustomExceptionsTestMethodCounter() {
        return customExceptionsTestMethodCounter;
    }

    public Map<String, Integer> getStandardOrThirdPartyExceptionsTestMethodCounter() {
        return standardOrThirdPartyExceptionsTestMethodCounter;
    }

    public Map<String, Integer> getNotIdentifiedExceptionsTestMethodCounter() {
        return notIdentifiedExceptionsTestMethodCounter;
    }

    public int getTotalNumberOfThrowStatementNotIdentifiedExceptions() {
        return totalNumberOfThrowStatementNotIdentifiedExceptions;
    }

    public int getTotalNumberOfCatchStatementNotIdentifiedExceptions() {
        return totalNumberOfCatchStatementNotIdentifiedExceptions;
    }

    public Map<String, Integer> getThrowsNotIdentifiedExceptionsCounter() {
        return throwsNotIdentifiedExceptionsCounter;
    }

    public Map<String, Integer> getCatchNotIdentifiedExceptionsCounter() {
        return catchNotIdentifiedExceptionsCounter;
    }

    public Map<String, Integer> getNotIdentifiedExceptionsUsed() {
        return notIdentifiedExceptionsUsed;
    }

    public Double getTotalAbsolutPercentegeOfNotIdentifiedExceptionsTestMethods() {
        return totalAbsolutPercentegeOfNotIdentifiedExceptionsTestMethods;
    }

    public Double getTotalAbsolutPercentegeOfStandardOrThirdPartyExceptionsTestMethods() {
        return totalAbsolutPercentegeOfStandardOrThirdPartyExceptionsTestMethods;
    }

    public Double getTotalAbsolutPercentegeOfStandardOrThirdPartyExceptionsTested() {
        return totalAbsolutPercentegeOfStandardOrThirdPartyExceptionsTested;
    }

    public Double getTotalAbsolutPercentegeOfNotIdentifiedExceptionsTested() {
        return totalAbsolutPercentegeOfNotIdentifiedExceptionsTested;
    }

    public Double getTotalRelativePercentegeOfStandardOrThirdPartyExceptionsTested() {
        return totalRelativePercentegeOfStandardOrThirdPartyExceptionsTested;
    }

    public Double getTotalRelativePercentegeOfNotIdentifiedExceptionsTested() {
        return totalRelativePercentegeOfNotIdentifiedExceptionsTested;
    }


}
