package exceptionhunter.models;

import com.google.gson.annotations.Expose;

/**
 * Armazena informações sobre as chamadas ao Throw.
 */
public class ThrowExceptionModel extends AbstractOrdinaryModel {

    //Armazena a exceção levantada
    @Expose
    private String exception;

    //TODO trocar Enum. Identifica o tipo de throw (NEW, RETHROW, STATIC_NEW, e outros)
    @Expose
    private String throwType;

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getThrowType() {
        return throwType;
    }

    public void setThrowType(String throwType) {
        this.throwType = throwType;
    }

    @Override
    public String toString() {
        return "ThrowExceptionModel{" +
                "exception='" + exception + '\'' +
                ", throwType='" + throwType + '\'' +
                '}' + " " + super.toString();
    }
}
