package exceptionhunter.models;

import com.google.gson.annotations.Expose;

/**
 * Identifica as exceções criadas no próprio projeto
 */
public class NewExceptionModel extends AbstractOrdinaryModel {
    //Identifica a classe mae
    @Expose
    String type = "NewExceptionModel";



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "NewExceptionModel{" +
                "type='" + type + '\'' +
                '}';
    }
}
