package exceptionhunter.models;

import com.google.gson.annotations.Expose;

/**
 * Salva informacoes sobre as instrucoes Throws dos métodos
 */
public class ThrowsExceptionModel extends AbstractOrdinaryModel {

    //Exceção informada pelo Throws
    @Expose
    private String exception;

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    @Override
    public String toString() {
        return "ThrowsExceptionModel{" +
                "exception='" + exception + '\'' +
                '}' + " " + super.toString();
    }
}
