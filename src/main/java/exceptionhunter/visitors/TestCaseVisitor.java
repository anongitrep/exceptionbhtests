package exceptionhunter.visitors;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import exceptionhunter.extractors.CombinedTypeExtractor;
import exceptionhunter.extractors.ModelExtractor;
import exceptionhunter.models.OrdinaryAnnotationTestModel;
import exceptionhunter.models.OrdinaryExtendedTestModel;

/**
 * Visitor que identifica novos testes filhas de TestCase ou que estejam com @Test. No caso das que herdam de TestCase
 * é necessário que a classe não possua a anotação @Test, pois isso já fará com que ela seja contabilizada como um
 * test por anotação.
 * <p>
 * Além disso, devido a um caso * onde a anotação @Test estava comentada por uma razão desconhecida, mas a classe e o
 * método estavam em um contexto de um teste real, comentários que possuam @Test tornarão o método imediatamente abaixo
 * do comentário classificado como um teste por anotação.
 */
public class TestCaseVisitor extends VoidVisitorAdapter<Void> {
    private CombinedTypeExtractor cte;
    private ModelExtractor me;
    private String rootPath;

    public TestCaseVisitor(CombinedTypeExtractor cte, ModelExtractor me, String rootPath) {
        this.cte = cte;
        this.me = me;
        this.rootPath = rootPath;
    }


    @Override
    public void visit(MethodDeclaration n, Void arg) {
        super.visit(n, null);
        boolean nameStartsWithTest = n.getNameAsString().startsWith("test");

        if (n.getAnnotationByName("Test").isPresent()) {
            OrdinaryAnnotationTestModel model = new OrdinaryAnnotationTestModel();
            model.setAnnotation("@Test");
            model.setRootPath(rootPath);
            model.setInsideATestMethod(true);
            model.setMethodNameStartsWithTest(nameStartsWithTest);
            me.extractBaseNodeData(n, model);
        } else if (n.getComment().isPresent() && n.getComment().get().toString().contains("@Test")) {
            OrdinaryAnnotationTestModel model = new OrdinaryAnnotationTestModel();
            model.setAnnotation("//@Test");
            model.setRootPath(rootPath);
            model.setInsideATestMethod(true);
            model.setMethodNameStartsWithTest(nameStartsWithTest);
            me.extractBaseNodeData(n, model);
        } else if (nameStartsWithTest && isTestCaseInheritance(n)) {
            OrdinaryExtendedTestModel model = new OrdinaryExtendedTestModel();
            model.setMethodNameStartsWithTest(nameStartsWithTest);
            model.setInsideATestMethod(true);
            model.setRootPath(rootPath);
            me.extractBaseNodeData(n, model);
        }

    }

    private boolean isTestCaseInheritance(MethodDeclaration n) {
        Node.ParentsVisitor pVisitor = new Node.ParentsVisitor(n);
        Node nextNode;
        while (pVisitor.hasNext()) {
            nextNode = pVisitor.next();
            if (nextNode instanceof ClassOrInterfaceDeclaration) {
                ClassOrInterfaceDeclaration node = (ClassOrInterfaceDeclaration) nextNode;
                for (ClassOrInterfaceType extendType : node.getExtendedTypes()) {
                    if (extendType.getNameAsString().contains("Test")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


}