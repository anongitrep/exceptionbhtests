package exceptionhunter.visitors.junit;

import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import exceptionhunter.extractors.CombinedTypeExtractor;
import exceptionhunter.extractors.ModelExtractor;
import exceptionhunter.models.junit.JUnit_ExpectExceptionTestCallModel;

/**
 * Visitor que identifica onde estao as chamadas ao metodo expect da classe ExpectedException. Esse metodo
 * é usado para definir exceções esperadas em alguns testes
 */
public class ExpectExceptionCallVisitor extends VoidVisitorAdapter<Void> {

    private CombinedTypeExtractor cte;
    private ModelExtractor me;
    private String rootPath;

    public ExpectExceptionCallVisitor(CombinedTypeExtractor cte, ModelExtractor me, String rootPath) {
        this.cte = cte;
        this.me = me;
        this.rootPath = rootPath;
    }

    @Override
    public void visit(MethodCallExpr n, Void arg) {
        super.visit(n, null);
        if (n.getNameAsString().equals("expect")) {
            //String qualifiedSignature = "";
            JUnit_ExpectExceptionTestCallModel model = new JUnit_ExpectExceptionTestCallModel();
            model.setRootPath(this.rootPath);
            me.extractBaseNodeData(n, model);
        }
    }
}