package exceptionhunter.visitors;

import com.github.javaparser.ast.stmt.CatchClause;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import exceptionhunter.extractors.CombinedTypeExtractor;
import exceptionhunter.extractors.ModelExtractor;
import exceptionhunter.models.CatchModel;

/**
 * Visitor que identifica e extrai as exceções esperadas dentro dos catchs
 */
public class CatchVisitor extends VoidVisitorAdapter<Void> {

    private CombinedTypeExtractor cte;
    private ModelExtractor me;
    private String rootPath;

    public CatchVisitor(CombinedTypeExtractor cte, ModelExtractor me, String rootPath) {
        this.cte = cte;
        this.me = me;
        this.rootPath = rootPath;
    }

    @Override
    public void visit(CatchClause n, Void arg) {

        super.visit(n, null);
        CatchModel model = new CatchModel();
        model.setRootPath(rootPath);
        me.extractBaseNodeData(n, model);
    }
}