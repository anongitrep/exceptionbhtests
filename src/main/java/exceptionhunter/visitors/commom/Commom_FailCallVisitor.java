package exceptionhunter.visitors.commom;

import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import exceptionhunter.extractors.CombinedTypeExtractor;
import exceptionhunter.extractors.ModelExtractor;
import exceptionhunter.models.commom.Commom_FailCallModel;

/**
 * Visitor que identifica todas as chamadas ao metodo fail.
 * Validado
 */
public class Commom_FailCallVisitor extends VoidVisitorAdapter<Void> {

    private CombinedTypeExtractor cte;
    private ModelExtractor me;
    private String rootPath;


    public Commom_FailCallVisitor(CombinedTypeExtractor cte, ModelExtractor me, String rootPath) {
        this.cte = cte;
        this.me = me;
        this.rootPath = rootPath;
    }

    @Override
    public void visit(MethodCallExpr n, Void arg) {

        super.visit(n, null);
        //Garante que o metodo fail é realmente da classe org.junit.Assert do JUnit.
        if (n.getNameAsString().equals("fail")) {
            //String qualifiedSignature = "";
            Commom_FailCallModel model = new Commom_FailCallModel();
            model.setRootPath(this.rootPath);
            me.extractBaseNodeData(n, model);
        }
    }
}

