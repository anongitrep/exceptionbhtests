package exceptionhunter.visitors;

import com.github.javaparser.ast.stmt.ThrowStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import exceptionhunter.extractors.CombinedTypeExtractor;
import exceptionhunter.extractors.ModelExtractor;
import exceptionhunter.models.ThrowExceptionModel;

/**
 * Visitor que identifica onde estão as instruções do tipo Throw new. Contudo, o Throw é usado de diversas formas
 * <p>
 * O total esta validado. Falta validar as excecoes de forma independente
 */
public class ThrowExceptionVisitor extends VoidVisitorAdapter<Void> {
    private CombinedTypeExtractor cte;
    private ModelExtractor me;
    private String rootPath;

    public ThrowExceptionVisitor(CombinedTypeExtractor cte, ModelExtractor me, String rootPath) {
        this.cte = cte;
        this.me = me;
        this.rootPath = rootPath;
    }

    @Override
    public void visit(ThrowStmt n, Void arg) {
        super.visit(n, null);
        //System.out.println(n.toString());
        ThrowExceptionModel model = new ThrowExceptionModel();
        model.setRootPath(rootPath);
        me.extractBaseNodeData(n, model);
    }
}