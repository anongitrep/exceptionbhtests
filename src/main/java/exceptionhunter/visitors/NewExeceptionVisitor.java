package exceptionhunter.visitors;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import exceptionhunter.extractors.CombinedTypeExtractor;
import exceptionhunter.extractors.ModelExtractor;
import exceptionhunter.models.NewExceptionModel;

/**
 * Visitor que identifica novas classes ou interfaces que sao filhas de alguma outra que possua "Exception" no nome
 * Regex para validar manualmente: \w* class \w* extends .*Exception.*
 * Validado
 */
public class NewExeceptionVisitor extends VoidVisitorAdapter<Void> {

    private CombinedTypeExtractor cte;
    private ModelExtractor me;
    private String rootPath;

    public NewExeceptionVisitor(CombinedTypeExtractor cte, ModelExtractor me, String rootPath) {
        this.cte = cte;
        this.me = me;
        this.rootPath = rootPath;

    }

    @Override
    public void visit(ClassOrInterfaceDeclaration n, Void arg) {
        super.visit(n, null);
        for (ClassOrInterfaceType extendType : n.getExtendedTypes()) {
            if (extendType.getNameAsString().contains("Exception") || extendType.getNameAsString().contains("Error") || extendType.getNameAsString().contains("Throwable")) {
                NewExceptionModel model = new NewExceptionModel();
                model.setRootPath(this.rootPath);
                if(extendType.getNameAsString().contains("Error")){
                    model.setIsAnError(true);
                }
                me.extractBaseNodeData(extendType, model);
            }
        }

    }
}