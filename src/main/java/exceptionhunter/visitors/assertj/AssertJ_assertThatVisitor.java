package exceptionhunter.visitors.assertj;

import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import exceptionhunter.extractors.CombinedTypeExtractor;
import exceptionhunter.extractors.ModelExtractor;
import exceptionhunter.models.assertj.AssertJ_assertThatModel;

/**
 * Visitor que identifica todas as chamadas ao metodo assertThrows. Esse metodo esta disponível apenas no Junit5
 */
public class AssertJ_assertThatVisitor extends VoidVisitorAdapter<Void> {
    private CombinedTypeExtractor cte;
    private ModelExtractor me;
    private String rootPath;

    public AssertJ_assertThatVisitor(CombinedTypeExtractor cte, ModelExtractor me, String rootPath) {
        this.cte = cte;
        this.me = me;
        this.rootPath = rootPath;
    }

    @Override
    public void visit(MethodCallExpr n, Void arg) {
        super.visit(n, null);
        if (n.getNameAsString().equals("assertThat")) {
            AssertJ_assertThatModel model = new AssertJ_assertThatModel();
            model.setRootPath(rootPath);
            this.me.extractBaseNodeData(n, model);
        }
    }
}