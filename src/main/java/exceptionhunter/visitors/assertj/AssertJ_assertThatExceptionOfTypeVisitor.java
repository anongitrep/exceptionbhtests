package exceptionhunter.visitors.assertj;

import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import exceptionhunter.extractors.CombinedTypeExtractor;
import exceptionhunter.extractors.ModelExtractor;
import exceptionhunter.models.assertj.AssertJ_assertThatExceptionOfTypeModel;

/**
 * Visitor que identifica todas as chamadas ao metodo assertThrows. Esse metodo esta disponível apenas no Junit5
 */
public class AssertJ_assertThatExceptionOfTypeVisitor extends VoidVisitorAdapter<Void> {
    private CombinedTypeExtractor cte;
    private ModelExtractor me;
    private String rootPath;

    public AssertJ_assertThatExceptionOfTypeVisitor(CombinedTypeExtractor cte, ModelExtractor me, String rootPath) {
        this.cte = cte;
        this.me = me;
        this.rootPath = rootPath;
    }

    @Override
    public void visit(MethodCallExpr n, Void arg) {
        super.visit(n, null);
        if (n.getNameAsString().equals("assertThatExceptionOfType")) {
            AssertJ_assertThatExceptionOfTypeModel model = new AssertJ_assertThatExceptionOfTypeModel();
            model.setRootPath(rootPath);
            this.me.extractBaseNodeData(n, model);
        }
    }
}