package exceptionhunter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class ExceptionHunterMain {

    public static void main(String[] args) throws IOException {
        Logger logger = LoggerFactory.getLogger("ExceptionHunter");


        boolean downloadOnly = false;

        String jsonPath = ProjectsManagerSingleton.class.getClassLoader().getResource("projects.json").getPath();


        if (args.length == 1){
            jsonPath = args[0];
            logger.info("Default JSON Path altered to " + jsonPath);
        }

        logger.info("Starting analysis of projects defined in  " + jsonPath);

        ProjectsConfiguration projectsConfiguration = ProjectsManagerSingleton.getProjectsManagerSingleton().getProjectsConfiguration(jsonPath);
        List<ProjectConfig> projects = projectsConfiguration.getProjects();

        logger.info("Save data to mongoDB? " + projectsConfiguration.isSaveToMongoDB());

        ProjectMain pjMain = new ProjectMain(projectsConfiguration);
        if(downloadOnly){
            pjMain.startDownload();
        } else {
            pjMain.proceedWithAnalysis();
        }

    }


}

